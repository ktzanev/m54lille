\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M54}
\usepackage{numprint}

% \solutionstrue

\begin{document}

\hautdepage{TD3 - Moments}


% -----------------------------------
\begin{exo}

  Le temps d'attente (en minutes) pour accéder à des données suit une loi uniforme $\mathcal{U}([1,6])$.
  \begin{enumerate}
    \item Déterminer la probabilité d'attendre au moins 5 minutes.
    \item Déterminer le temps d'attente moyen.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  \begin{enumerate}
    \item Soit $X$ une variable aléatoire réelle de loi uniforme sur $[1,3]$. Calculer $\EE{X}$ et $\EE{X^2}$.
    \item Soit $X$ une variable aléatoire réelle de loi uniforme sur $[0,\pi]$. Calculer $\EE{\sin(X)}$ et $\EE{\cos(X)}$.
    \item Soit $X$ une variable aléatoire réelle de loi exponentielle $\mathcal{E}(\lambda)$. Calculer $\EE{e^{X/2}}$ lorsqu'elle existe.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $X$ une variable uniforme sur $[1,3]$ et $a\in[1,3]$.
  \begin{enumerate}
    \item Quelle est la loi de la variable aléatoire  $Y=\min\{X,a\}$ ?
    \item Admet-elle une espérance ? Si oui, la calculer.
    \item Que vaut cette espérance si $a=1$ ? $a=3$ ? Est-ce que vous auriez pu trouver ces deux résultats autrement ?
  \end{enumerate}
\end{exo}

\begin{solution}
  On trouve $F_Y(x)=\ind_{x\geq a}+\frac{x-1}{2}\ind_{x<a}$ $\mathbb{P}(Y>x)=\frac{3-x}{2}\ind_{1\leq x<a}+\ind_{0\leq x<1}$ d'où $\EE{Y}=\frac{-1}{4}+\frac{3a}{2}-\frac{a^2}{4}$
\end{solution}


% -----------------------------------
\begin{exo} (Deux lois sans mémoire)

  Soit $X$ une variable aléatoire qui suit la loi géométrique de paramètre $p$:
  \[
  \mathbb{P}(X=k)=p(1-p)^{k-1}\quad\forall k\in\mathbb{N}^{*}
  \]
  \begin{enumerate}
    \item Calculer la fonction de répartition de $X$, d'abord pour $x\in \mathbb{N}$, puis pour $x$ quelconque. Pensez à bien vérifier qu'il s'agit d'une fonction de répartition.
    \item En utilisant la fonction de répartition de $X$, calculer $E(X)$.
    \item Pour $j$ et $k$ dans $\mathbb{N}^*$, comparer $\PP{X>j}$ et $\PP{X>j+k \mid X>k}$. Que remarque-t-on ? On appelle ce type de lois des lois sans mémoire.
  \end{enumerate}
  Le temps d'utilisation d'une ampoule avant qu'elle grille est une variable aléatoire $T$ qui suit la loi exponentielle de paramètre $a=\numprint{0.005}$ (jours$^{-1}$).
  \begin{enumerate}[resume]
    \item Montrer que $T$ a une espérance et la calculer.
    \item Pour $s,t\in \mathbb{R}_+$, calculer la probabilité $\PP{T>t}$ que l'ampoule soit encore allumée au bout de $t$ jours, et la probabilité $\PP{T>t+s \mid T>s}$ que l'ampoule dure encore $t$ jours de plus si on constate au bout de $s$ jours qu'elle est toujours allumée. Que remarque-t-on ?
    \item On allume l'ampoule, on contrôle une fois par jour si elle est toujours allumée, et on note $Z$ le nombre de jours au bout duquel on constate qu'elle a grillé. Ainsi, on pose $Z=1$ si on trouve l'ampoule grillée au soir du premier jour, $Z=2$ si cela se produit au soir du deuxième jour, etc. Déterminer la loi de $Z$. Quelle loi reconnaissez-vous ?
  \end{enumerate}
\end{exo}

\begin{solution}

  Pour la loi géométrique, la fonction de survie est $(1-p)^k$ et l'espérance $1/p$.

  Pour la loi exponentielle, il est encore beaucoup plus simple de passer par la fonction de survie pour calculer l'espérance. On reconnaît la loi géométrique de paramètre $p=1-e^{-a}$.
\end{solution}


% -----------------------------------
\begin{exo}\label{exo:Chernoff} (Inégalité de Chernoff)

  \begin{enumerate}
    \item Soit $X$ une variable aléatoire réelle sur $(\Omega,\mathcal{F},P)$ telle que pour un certain $a>0$, $\EE{\eexp^{aX}}$ soit finie. Montrer que
    \begin{equation}\label{Chernoff}
      \forall t\in\mathbb{R},\quad P(X\geq t)\leq \eexp^{-at}\EE{\eexp^{aX}}.\tag{Chernoff}
    \end{equation}
  \end{enumerate}
    Cette inégalité s'appelle \emph{inégalité de Chernoff}. Dans les cas où l'on sait calculer explicitement $\EE{\eexp^{aX}}$, on peut alors chercher à optimiser le majorant dans \eqref{Chernoff} en choisissant le meilleur $a$ possible.
  \begin{enumerate}[resume]
    \item Calculer $\EE{\eexp^{aX}}$ et en déduire un majorant optimal (par cette méthode) lorsque $X$ suit  :
    \begin{enumerate}
      \item une loi normale centrée réduite;
      \item une loi de Poisson de paramètre 1;
      \item une loi exponentielle de paramètre 1;
      \item une loi géométrique de paramètre $1/2$.
    \end{enumerate}
  \end{enumerate}
\end{exo}

\begin{solution}

  Ne faire que les deux premières lois si on manque de temps.
  \begin{enumerate}
    \item $\exp(-t^2/2)$
    \item $\exp(-t\ln(t)+t-1)$
    \end{enumerate}
\end{solution}

% -----------------------------------
\begin{exo} (Consommation d'eau)

  La consommation journalière en eau  d'une agglomération au cours du mois de juillet est une variable aléatoire $X$ dont la densité $f$ a la forme:
  \[
    f(t)=c(t-a)(b-t)\ind_{[a,b]}(t),\quad t\in\mathbb{R},
  \]
  où $a$, $b$, $c$ sont des constantes strictement positives ($a<b$).
  \begin{itemize}
    \item Vérifier que l'on a pour tout $n\in\mathbb{N}$:
    \[
       \int_a^b (t-a)^n(b-t)\,dt=\frac{(b-a)^{n+2}}{(n+1)(n+2)}.
    \]
    \item Exprimer la constante $c$ en fonction de $a$ et $b$.
    \item Calculer $\EE{X-a}$ et $\EE{(X-a)^2}$. En déduire $\EE{X}$ et $\VV{X}$.
    \item Donner la fonction de répartition $F$ de la variable aléatoire $X$: on distinguera pour le calcul de $F(x)$ les cas $x<a$, $a\leq x\leq b$ et $x>b$ et, dans le deuxième cas, on écrira $F(x)$ en fonction de $(x-a)$ et $(b-x)$ sans développer ni réduire le polynôme obtenu. Donner l'allure des représentations graphiques de $f$ et $F$. Proposer une interprétation physique des constantes $a$ et $b$.
  \end{itemize}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item $(b-a)/2$, $(b-a)^2*3/10$.
    \item $(b+a)/2$, $(b-a)^2/20$.
    \item $(x-a)^3/(b-a)^3(3b-2x-a)$
    \item $a$ et $b$ sont les minimum et maximum de la consommation journalière.
  \end{enumerate}
\end{solution}

% -----------------------------------
\begin{exo}

  On munit $(\mathbb{R}^2,\mathrm{Bor}(\mathbb{R}^2))$ de la loi uniforme sur $[0,1]^2$. Et on définit les variables suivantes :
  \begin{itemize}
    \item $U(\omega_1,\omega_2)=\max\{\omega_1,\omega_2\}$,
    \item $V(\omega_1,\omega_2)=\min\{\omega_1,\omega_2\}$,
    \item $D(\omega_1,\omega_2)=\left|\omega_1-\omega_2\right|$,
  \end{itemize}
  pour $(\omega_1,\omega_2)\in\mathbb{R}^2$.
  \begin{enumerate}
    \item Déterminer la loi de ces variables aléatoires.
    \item Calculer leur espérance si elle existe.
  \end{enumerate}
\end{exo}

\begin{solution}

  Calculer les fonctions de répartitions. Pour la troisième: faire un dessin: il est très facile de représenter les aires sur le graphe. La solution avec les intégrales est beaucoup plus compliquée.
  \begin{enumerate}
    \item $x^2$, $2/3 $
    \item $1-(1-x)^2$, $1/3$,
    \item $1-(1-x)^2$, $1/3$.
  \end{enumerate}
\end{solution}

% -----------------------------------
\begin{exo}

  Soit $F$ la fonction définie sur $\mathbb{R}$ par
  \[
    f(x)=
    \begin{cases}
      1-(1+\frac x2)e^{-\frac x2} & \text{si } x>0,\\
      0                           & \text{sinon }.
    \end{cases}
  \]
  \begin{enumerate}
    \item Montrer que $F$ est la fonction de répartition d'une loi de probabilité dont on déterminera la densité si elle existe.
    \item Si $X$ est une variable aléatoire de fonction de répartition $F$, calculer la probabilité $\PP{-2<X<3}$. Que pouvez-vous dire du signe de $X$ ? Calculer l'espérance de $X$.
  \end{enumerate}
\end{exo}

\begin{solution}

  $F$ est $C^0$ sur $\mathbb{R}$, $C^{\infty}$ sur $]0,\infty[$ et sur $]-\infty,0[$. De plus, ses dérivées sur $\mathbb{R}^{*}_{+}$ et $\mathbb{R}^{*}_{-}$ admettent des limites finies en 0. La fonction $F$ est donc dérivable. \\ Dérivée $x/2e^{-x/2}$. Espérance 1.
\end{solution}

% -----------------------------------
\begin{exo}

  \begin{enumerate}
    \item  Montrer que la fonction
    \[
      f(x)=\frac{1}{\pi\sqrt{1-x^2}}\ind_{]-1,1[}(x),
    \]
    définit une densité de probabilité sur $\mathbb{R}$.
    \item Donner la fonction de répartition et l'espérance (si elle existe) d'une variable aléatoire $X$ de densité $f$ sur $\mathbb{R}$. \item Comment simuler une telle variable aléatoire ?
    \item Si $T$ suit une loi de Cauchy, montrer que la variable aléatoire $\displaystyle Z=\frac{1-T^2}{1+T^2}$ a pour densité $f$. On rappelle que la loi de Cauchy a pour densité:
    \[
      f(t)=\frac{1}{\pi(1+x^2)}
    \]
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Changement de variable $x=\sin(y)$ pour montrer que l'intégrale vaut 1.
    \item $\arcsin(x)+\pi/2$. 0.
    \item $\sin(U-\pi/2)$.
  \end{enumerate}
\end{solution}

% -----------------------------------
\begin{exo} (Moments de la loi normale)\label{moments_gaussienne}

  Soit $X$ une variable aléatoire normale centrée réduite.
  \begin{enumerate}
    \item Que valent $\EE{X^{2n+1}}$ pour $n\in\mathbb{N}$ ?
    \item Pour $n\in\mathbb{N}$, on pose $c_n=\EE{X^{2n}}$. Montrer que $c_n=(2n-1)c_{n-1}$ pour $n\in\mathbb{N}^*$. En déduire une formule explicite pour $\EE{X^{2n}}$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo} (Peut-on utiliser le théorème de Beppo Levi avec une suite décroissante ?) \label{exo-th-cvdec}

  La réponse à cette question provocatrice est fournie par le théorème suivant.\\
  \textbf{Théorème de convergence décroissante}\hspace*{1mm}
  \begin{quote}
  Soit $(X_n)_{n\ge 1}$ une suite \emph{décroissante} de v.a. positives
    définies sur le même espace probabilisé $(\Omega,\mathcal{F},P)$.  On suppose de
    plus que $\E X_1<+\infty$. Alors la suite $(\E X_n)_{n\ge 1}$ converge en
    décroissant vers $\E X$, où la v.a.  $X$ est définie par
    $X(\omega)=\lim_{n\to +\infty}X_n(\omega)$, pour tout $\omega\in\Omega$.
  \end{quote}
  \begin{enumerate}
    \item Démontrez ce théorème en appliquant le théorème de Beppo Levi à une suite \emph{croissante} convenablement construite.
    \item Soit $U$ une variable aléatoire sur $(\Omega,\mathcal{F})$, à valeurs dans $]0,1]$ et de loi uniforme sur $]0,1]$. On considère la suite de v.a. $(X_n)_{n\ge 1}$ définie sur  $(\Omega,\mathcal{F})$ par
    \[
      X_n = \frac{1}{U}\ind_{]0,1/n]}(U).
    \]
    Vérifiez qu'elle est décroissante et converge simplement vers $0$ sur tout $\Omega$. Que vaut $\E X_n$ ? Quel est l'intérêt de cette question ?
  \end{enumerate}
\end{exo}


% ==================================
\section{Entraînement supplémentaire}
% ==================================


% -----------------------------------
\begin{exo} (Moments de la loi exponentielle)

  Pour $a$ un réel strictement positif, et $n$ un entier positif, on pose
  \[
    i_n(a)=  \int_0^{+\infty} x^n a \eexp^{-ax} \dd x.
  \]
  \begin{enumerate}
    \item Etablir une relation de récurrence entre $I_n(a)$ et $I_{n-1}(a)$.
    \item Que vaut $I_0(a)$ ?
    \item En déduire $I_n(a)$.
    \item Soit $X$ une variable aléatoire réelle de loi exponentielle $\mathcal{E}(1)$. Calculer $\EE{X^n}$ pour $n$ un entier positif.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo} (Espérance et variance d'une loi de Pareto)

  Soit $X$ une variable aléatoire de loi de Pareto de paramètre $a>0$ ; $X$ admet donc pour densité la fonction $f$ donnée par
  \[
    f(t)=\frac{a}{t^{a+1}}\ind_{[1,+\infty[}(t).
  \]
  \begin{enumerate}
    \item Montrer que $X$ est une variable aléatoire positive presque sûrement.
    \item Calculer son espérance en fonction du paramètre $a$.
    \item Calculer sa variance en fonction du paramètre $a$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Tous les matins, Monsieur T. sort de chez lui à un instant "au hasard" entre 7h et 7h30. Il peut prendre le bus de 7h15 ou celui de 7h30, qui le dépose 5mn plus tard à la station de métro la plus proche. Mais comme la station n'est qu'à 10mn de marche de son domicile, il préfère y aller à pied si le temps qu'il doit passer à attendre le bus est supérieur à 5mn. On note $X$ le temps de trajet de Monsieur T. entre son domicile et le métro, et $Y$ le temps qui s'écoule entre l'instant initial (7h) et l'arrivée de Monsieur T. à la station de métro.
  \begin{enumerate}
    \item Donner l'expression et tracer le graphe des fonctions de répartitions de $X$ et de $Y$.
    \item Calculer la somme des $\PP{X=x}$ pour tous les $x\in\mathbb{R}$ et la somme des $\PP{Y=y}$ pour tous les $y\in\mathbb{R}$. Ces variables aléatoires sont-elles à densités ? Sont-elles discrètes ?
    \item Lire sur les graphes les valeurs des probabilités $\PP{X<5}$, $\PP{X<8}$, $\PP{X<10}$, $\PP{Y<15}$, $\PP{Y<20}$, $\PP{Y<22}$, $\PP{Y\in\,]20;30[\,}$ et  $\PP{Y=35}$.
    \item En moyenne, combien de temps Monsieur T. met-il à rejoindre la station de métro et à quelle heure y arrive-t-il ?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $f$ la fonction définie sur $\mathbb{R}$ par
  \[
    f(x) =
      \begin{dcases}
        \frac{a}{\left|x\right|^5} & \text{si } x \notin ]-1,1[\\
        0                          & \text{si } x \in ]-1,1[.
      \end{dcases}
  \]
  \begin{enumerate}
    \item Déterminer $a$ pour que $f$ soit une densité de probabilité sur $\mathbb{R}$.
    \item Soit $X$ une variable aléatoire réelle de densité $f$. Calculer $\E X$ et $\V X$, si ces quantités existent.
    \item Peut-on trouver deux réels $\alpha$ et $\beta$ pour que la variable aléatoire $Y= \alpha X + \beta$ soit centrée et réduite ?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $\alpha>0$. Montrer que l'intégrale
  \[
    \int_0^{+\infty}t^{\alpha-1}e^{-t}\dd t
  \]
  est finie. On la note $\Gamma(\alpha)$.
  \begin{enumerate}
    \item Montrer que $\Gamma(\alpha+1)=\alpha\Gamma(\alpha)$, $\Gamma(n)=(n-1)!$ pour tout entier $n$ non nul.
    \item Donner la valeur de $\Gamma(1/2)$. %=\sqrt{\pi}$.
    \item En déduire la valeur de
    \[
      \frac{1}{\sqrt{2\pi}}\int_{\mathbb{R}}|x|^ne^{-\frac{x^2}{2}}\dd x,
    \]
    pour tout entier $n$ (comparer avec les résultats de l'exercice \ref{moments_gaussienne}).
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.49]

  Soit $X$ une variable aléatoire de loi uniforme sur $[-1,1[$.
  \begin{enumerate}
    \item Quelle est la loi de $|X|$, $X^2$, et $\frac12 \ln\left(\frac{1+X}{1-X}\right)$ ?
    \item Calculer leur espérance si elle existe.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $X$ une variable aléatoire à valeurs dans $\mathbb{R}$ dont la densité est
  \[
     f(x)=\frac{x+\ln x}{x^2}\ind_{[1,a]}(x),
  \]
  pour $a>1$.
  \begin{enumerate}
    \item Montrer que $a$ est défini de façon unique et que $a<e$.
    \item Quelle est la fonction de répartition de $X$ et son espérance ?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo} (Contrôle de la queue gaussienne)

  Soit $X$ une variable aléatoire normale centrée réduite.
  \begin{enumerate}
    \item\label{q:optChernoff} Calculer $\EE{\eexp^{aX}}$ pour $a>0$. En optimisant l'inégalité \eqref{Chernoff} de l'exercice \ref{exo:Chernoff} par rapport à $a$ pour $t>0$ fixé, en déduire une majoration de $\PP{X \geq t}$.
    \item On cherche à améliorer la majoration trouvée en 1). Par une étude de fonction, déterminer
    \[
      \sup_{t > 0} \left\{ P(X \ge t) -\frac{1}{t \sqrt{2\pi}} \eexp^{-t^2/2} \right\}.
    \]
    \item\label{q:optChernoff2} En déduire une nouvelle majoration de $\PP{X \ge t}$ pour $t > 0$. Est-elle meilleure que celle trouvée en \eqref{q:optChernoff} ?
    \item Déterminer
    \[
      \sup_{t > 0} \left\{ P(X \ge t) \eexp^{t^2/2} \right\}.
    \]
    En déduire une troisième majoration de $\PP{X \ge t}$ pour $t > 0$ et la comparer à celle trouvée en \eqref{q:optChernoff}.\\
    \begin{indication}
      On pourra étudier les variations de $h(t) = P(X \ge t) \eexp^{t^2/2}$ en se servant du résultat de \eqref{q:optChernoff2}.
    \end{indication}
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.49]

  Soit $X$ une variable aléatoire de loi normale centrée réduite et $n$ un entier supérieur ou égal à $2$. Pour quelles valeurs de $a\in\mathbb{R}^*$, la probabilité $\PP{a<X<na}$ est-elle maximale ?
\end{exo}


\end{document}
