\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M54}
\usepackage{numprint}

% \solutionstrue

\begin{document}

\hautdepage{TD2 - Variables aléatoires}


% ==================================
\section{Variables aléatoires discrètes}
% ==================================


% -----------------------------------
\begin{exo}(Contrôleur contre fraudeur)

Une compagnie de métro pratique les tarifs suivants. Le ticket donnant droit à un trajet coûte 1\,€; les amendes sont fixées à 20\,€ pour la première infraction constatée, 40\,€ pour la deuxième et \numprint{400}\,€ pour la troisième. La probabilité $p$ pour un voyageur d'être contrôlé au cours d'un trajet est supposée constante et connue de la seule compagnie ($0<p<1$). Un fraudeur décide de prendre systématiquement le métro sans payer jusqu'à la deuxième amende et d'arrêter alors de frauder. On note $T$ le nombre de trajets effectués jusqu'à la deuxième amende ($T$ est le numéro du trajet où le fraudeur est contrôlé pour la deuxième fois). On note $q=1-p$ la probabilité de faire un trajet sans contrôle.

\begin{enumerate}
  \item Montrer que la loi de $T$ est donnée par
  \[
    \PP{T=k}=(k-1)p^2q^{k-2},\quad k\geq 2.
  \]
  \item Pour $n\in\mathbb{N}^\star$, calculer $\PP{T>n}$.
  \begin{indication}
    On pourra commencer par chercher une formule explicite pour la somme de la série entière
    \[
      f(x) \coloneqq \sum_{k=n+1}^{+\infty}x^{k-1},
    \]
    puis pour sa dérivée terme à terme.
  \end{indication}
  \item Calculer numériquement $\PP{T>60}$ (pourquoi s'intéresse-t-on à cette quantité~?) lorsque $p=1/10$ et lorsque $p=1/20$.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Pour $1\leq j< k$, notons $E_{j,k}$ l'événement: \enquote{le premier contrôle du fraudeur a lieu lors de son $j$-ème trajet et le deuxième contrôle lors du $k$-ième trajet}. On a clairement la décomposition en union disjointe
    \begin{equation}\label{041102ca}
      \forall k\geq 2,\quad \{T=k\}=\bigcup_{j=1}^{k-1}E_{j,k}.
    \end{equation}
    Par additivité de la probabilité, le calcul de $\PP{T=k}$ se ramène donc à celui des $\PP{E_{j,k}}$. Notons $F_i$ l'événement \enquote{le fraudeur n'est pas contrôlé lors de son $i$-ème trajet}. Son complémentaire $F_i^c$ est donc l'événement \enquote{le fraudeur est contrôlé lors de son $i$-ème trajet}. L'événement $E_{j,k}$ peut  s'écrire sous la forme
    \[
      E_{j,k}= F_j^c \cap F_k^c\cap
      \Bigl(\bigcap_{\substack{1\leq i<k\\ i\neq j}}F_i\Bigr) ,
    \]
    avec le cas particulier $E_{1,2}= F_1^c \cap F_2^c$. Nous faisons maintenant l'hypothèse d'indépendance\,\footnote{Cette hypothèse revient pratiquement à dire que les contrôles sont faits au hasard, sans tenir compte des résultats des contrôles précédents. Elle ne serait pas pertinente si par exemple, le fraudeur était pris en filature par un contrôleur particulièrement hargneux et décidé à le coincer.} de la suite d'événements $(F_i)_{i\in\mathbb{N}^\star}$. Alors les $k$ événements $F_i$ ($i\neq j,k$) et $F_j^c$, $F_k^c$ sont mutuellement indépendants  et
    \[
      \PP{E_{j,k}}=\PP{F_j^c}P(F_k^c)\prod_{\substack{1\leq i<k\\ i\neq j}}\PP{F_i}
      =p^2 q^{k-2}.
    \]
    On note au passage que $\PP{E_{j,k}}$ ne dépend pas de $j$. En utilisant l'additivité de $P$ et (\ref{041102ca}), on aboutit à:
    \[
      \forall k\geq 2,\quad
      \PP{T=k}=\sum_{j=1}^{k-1}\PP{E_{j,k}}=(k-1)p^2q^{k-2}.
    \]
    La variable aléatoire $T$ suit ainsi la loi binomiale négative de paramètres $2$ et $p$. C'est la loi du temps d'attente du deuxième succès (ici pour le contrôleur!) dans une suite d'épreuves répétées indépendantes.
    \item La probabilité $\PP{T>n}$ s'exprime à l'aide des $\PP{T=k}$ par $\sigma$-additivité:
    \[
      \PP{T>n}=\sum_{k=n+1}^{+\infty}\PP{T=k}
      =p^2\sum_{k=n+1}^{+\infty}(k-1)q^{k-2}.
    \]
    On voit ainsi que
    \begin{equation}\label{031102ab}
      \PP{T>n}=p^2f_n'(q),
    \end{equation}
    où la fonction $f_n$ est définie comme la somme de la série entière
    \[
      f_n(x) \coloneqq \sum_{k=n+1}^{+\infty}x^{k-1}.
    \]
    Cette série est une série géométrique de raison $x$, donc de rayon de convergence $R=1$. On sait alors que sa somme est dérivable sur l'intervalle $]-R,R[$ et que sa dérivée peut se calculer par dérivation terme à terme:
    \[
      \forall x\in]-1,1[,\quad
      f'_n(x) \coloneqq \sum_{k=n+1}^{+\infty}(k-1)x^{k-2}.
    \]
    Comme $0<p<1$ (sinon le problème est sans intérêt), $q \coloneqq 1-p$ est aussi dans l'intervalle $]0,1[$ et on peut appliquer l'égalité ci-dessus avec $x=q$. Par ailleurs, on peut calculer explicitement $f_n(x)$ comme somme d'une série géométrique:
    \[
      f_n(x)=x^n+x^{n+1}+x^{n+2}+\dots\dots=x^n\sum_{j=0}^{+\infty}x^j
      =\frac{x^n}{1-x},
    \]
    formule valable pour tout $x\in]-1,1[$. On en déduit une formule explicite pour la dérivée:
    \[
      f'_n(x)=\frac{(1-x)nx^{n-1}+x^n}{(1-x)^2}=
      \frac{(n-(n-1)x)x^{n-1}}{(1-x)^2}.
    \]
    En faisant $x=q$ (donc $1-x=p$) et en reportant ce résultat dans (\ref{031102ab}), il vient:
    \[
      \PP{T>n}=(n-(n-1)q)q^{n-1}=(np+q)q^{n-1}.
    \]
    \item Le bilan financier de la stratégie du fraudeur est la variable aléatoire $S \coloneqq T-60$. En effet lorsqu'il arrête de frauder après son deuxième contrôle, il a dépensé en tout $60$\,€ d'amende et gagné $T$\,€ en ne payant pas ses tickets de métro. La probabilité que sa stratégie soit bénéficiaire est donc $\PP{S>0}=\PP{T>60}$. Le calcul numérique donne
    \[
      \PP{T>60}\simeq
      \begin{cases}
      \numprint{0,0138} & \text{pour $p=1/10$};\\
      \numprint{0,1916} & \text{pour $p=1/20$.}
      \end{cases}
    \]
  \end{enumerate}
\end{solution}


% ==================================
\section{Autour de la fonction de répartition}
% ==================================


% -----------------------------------
\begin{exo} (Interprétation du graphique d'une f.d.r. \emph{(Adapté de l'examen 01/05)})

  La variable aléatoire $X$ a pour fonction de répartition  $F$ représentée sur la figure
  \begin{center}
    \includegraphics{M54_2018-19_TD2_fig1}
  \end{center}
  \begin{enumerate}
  \item En exploitant les informations fournies par ce graphique, donner les valeurs des probabilités suivantes.
  \[
    \begin{array}{llll}
      \PP{X\leq -1},\quad        &  \PP{X=\numprint{0,2}},\quad       & \PP{X=\numprint{0,3}},\quad &
      \PP{X\geq \numprint{0,2}}, \\ [0.5ex]
      \PP{X>2},\quad             &  \PP{X\in[1;\numprint{1,5}]},\quad & \PP{X\in[1;2]},\quad        &
      \PP{\abs{X}>1}.
    \end{array}
  \]
  \item La variable aléatoire $X$ est-elle à densité ?
  \item Calculer la somme des sauts de $F$. La variable aléatoire $X$ est-elle discrète ?
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item
    \[
      \begin{array}{llll}
        \PP{X\leq -1}=0,1;\quad        &  \PP{X=\numprint{0,2}}=0,15;\quad    & \PP{X=\numprint{0,3}}=0;\quad & \PP{X\geq \numprint{0,2}}=0,7; \\ [0.5ex]
        \PP{X>2}=0,3;\quad             &  \PP{X\in[1;\numprint{1,5}]}=0;\quad & \PP{X\in[1;2]}=0,1;\quad      & \PP{\abs{X}>1}=0,5.
      \end{array}
    \]
    \item Si la loi de $X$ était à densité, sa f.d.r. serait continue. Or $F$ est discontinue aux points $0,2$ et $2$. Donc, la loi de $X$ n'est pas à densité.
    \item La fonction $F$ admet deux sauts d'amplitude $0,15$ et $0,1$. Donc la somme ($0,25$) ne fait pas $1$. Donc la loi de $X$ n'est pas discrète et donc $X$ ne peut-être une v.a. discrète.
  \end{enumerate}
\end{solution}


% -----------------------------------
\begin{exo}

  Les fonctions suivantes sont-elles des fonctions de répartition d'une variable aléatoire réelle ?
  \begin{align*}
    F(x) & = \sin (x), \quad G(x) = \frac{1}{\pi}\big(\arctan (x) + \frac{\pi}{2}\big), \quad H(x) = \frac{1}{4} \ind_{[-1,0[}(x) +  \frac{3}{4} \ind_{[0,1]}(x) + \ind_{]1,+\infty[}(x),\\
    J(x) & = \frac{1}{6} \big(\ind_{[1,2[}(x) + 2\,\ind_{[2,3[}(x) + 3\,\ind_{[3,4[}(x) + 4\,\ind_{[4,5[}(x) + 5\,\ind_{[5,6[}(x) + 6\,\ind_{[6, +\infty[}(x)\big).
  \end{align*}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $X$ une variable aléatoire réelle de loi uniforme sur $[0,1]$. Déterminer la loi de la variable aléatoire $Y$ dans les cas suivants :
  \begin{enumerate}
    \item $Y=1-X$ ;
    \item $Y=a+(b-a)X$, où $a$ et $b$ sont deux réels tels que $a<b$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $X$ une variable aléatoire réelle de fonction de répartition $F_X$. On pose $Z=\min (X,c)$ où $c$ est un réel.
  \begin{enumerate}
    \item Calculer la fonction de répartition de $Z$.
    \item Si la loi de $X$ a pour densité $f$, est-ce que la loi de $Z$ est encore à densité ?
  \end{enumerate}
\end{exo}


% ==================================
\section{Variables aléatoires à densité}
% ==================================


% -----------------------------------
\begin{exo} (Interprétation du graphique d'une densité)

  La variable aléatoire $X$ a pour densité la fonction $f$ représentée sur la figure
  \begin{center}
    \includegraphics{M54_2018-19_TD2_fig2}
  \end{center}
  \begin{enumerate}
    \item En exploitant les informations fournies par ce graphique, donner les
    valeurs des probabilités suivantes:
    \[
      \begin{array}{lll}
        \PP{X\leq -2},\quad & \PP{X=\numprint{-1}},\quad & \PP{X\in[-2;0]},\\
        \PP{X>1},           & \PP{X \ge 1},              & \PP{|X| > 1}.
      \end{array}
    \]
    \item Déterminer la fonction de répartition de $X$.
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}

  Soit $X$ une v.a.r. de fonction de répartition $F$ donnée par
  \[
    \forall u \in \mathbb{R}, \quad
    F(u) = \int_{-\infty}^u f(t) \dd t,
      \quad \text{avec} \quad
    f(t)=
      \begin{cases}
          1+t    & \text{si } t\in[-1,0],\\
          \alpha & \text{si } t\in]0,2],\\
          0      & \text{sinon.}
      \end{cases}
  \]
  \begin{enumerate}
    \item Représenter $f$.
    \item Calculer $F$, et en déduire $\alpha$.
    \item Représenter $F$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo} (Apnée)

  Dans cet exercice, on s'intéresse à l'\emph{apnée statique}, qui consiste à rester immobile immergé dans une piscine. Un individu \enquote{quelconque} va à la piscine, et s'entraîne à l'apnée. On appelle $T$ la durée (en minutes) maximale d'apnée statique qu'il réalise\footnote{Le record d'apnée statique est à 11'35'' pour les hommes, 9'02''pour les femmes.}. On suppose que la loi de $T$ est donnée par la fonction de répartition suivante :
  \[
    \forall t \in \mathbb{R}, \quad
    \mathbb{P}(T \le t) = \int_{-\infty}^t f(s) \dd s,
      \quad \text{avec} \quad
    f(s) =
      \begin{cases*}
        0               & si $s<0$ ou $s \geq 10$, \\
        \lambda s(10-s) & si $0 \leq s < 10$,
      \end{cases*}
  \]
  où $\lambda$ est un réel strictement positif.
  \begin{enumerate}
    \item Donner l'allure du graphe de la fonction $f$.
    \item Calculer la fonction de répartition de $T$. En déduire $\lambda$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo} (Loi de Rayleigh \emph{[issu du partiel 2011]})

  Soit $U$ une variable aléatoire réelle suivant la loi uniforme sur l'intervalle $]0,1]$.
  \begin{enumerate}
    \item Rappeler la fonction de répartition $F_U$ de $U$.\\
    Soit $\sigma$ un réel strictement positif, on définit une nouvelle variable aléatoire réelle, $X$, par
    \[
      X= \sigma \sqrt{-2 \ln U}.
    \]
    \item Calculer la fonction de répartition de $X$, $F_X$.
    \item La variable aléatoire $X$ est-elle à densité ? Si oui, donner une densité de $X$.
    \item Que peut-on en déduire sur la valeur de l'intégrale $\displaystyle{\int_{[0, +\infty[} x \, \eexp^{-x^2/2\sigma^2} \dd \lambda_1(x)}$ ?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo} (Un peu de trigonométrie aléatoire)

  On définit une variable aléatoire $X$ grâce à la construction représentée à la figure plus bas. L'angle $\big(\overrightarrow{AO},\overrightarrow{AM}\big)$ a pour mesure en radians $U$, \emph{variable aléatoire} de loi uniforme sur $]-\pi/2,\pi/2[$. La distance $AO$ vaut $1$ et $X$ est l'abscisse du point $M$ sur la droite de repère $(O,\vec{i}\ )$: $\overrightarrow{OM}=X\vec{i}$.  Les angles sont orientés dans le sens trigonométrique, la figure est faite pour $U$ positif. Pour $U=0$, $M$ coïncide avec le point $O$ et pour $-\pi/2<U<0$, $M$ est \enquote{à gauche} de $O$.
  \begin{center}
    \includegraphics{M54_2018-19_TD2_fig3}
  \end{center}
  \begin{enumerate}
    \item Exprimez $X$ en fonction de $U$.
    \item Pour $x$ réel, calculez $\PP{X\le x}$. On obtient ainsi la fonction de répartition de la variable aléatoire réelle $X$.
    \item Expliquez pourquoi la loi de $X$ est à densité et calculez cette densité. Que reconnaissez vous ainsi ?
    \item Quelle est la valeur de $\PP{\abs{X}\le 1}$ ? Cette question peut se résoudre avec ou sans l'aide des précédentes.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item En utilisant le triangle rectangle $AOM$ et en tenant compte de l'orientation, on obtient:
    \[
      X=\tan U.
    \]
    En effet si $U$ est positif, l'angle aigu non orienté $OAM$ a pour mesure $U$ et $OM=X$. Or $\tan(OAM)=\frac{OM}{OA}$. Si $U$ est négatif, $OAM$ a pour mesure $-U=\abs{U}$, $X=-OM$ donc la relation $\tan(OAM)=\frac{OM}{OA}$ donne $\tan(-U)=-X$ d'où $\tan U = X$ car la fonction tangente est impaire.

    Une autre façon de vérifier la formule $X=\tan U$ est de tracer le cercle \enquote{trigonométrique} de centre $A$ et de rayon $1$, en prenant l'origine des angles au point $O$ et d'utiliser l'interprétation géométrique de la fonction tangente. Par retrouver la figure à laquelle vous êtes habitués (?), il vous suffit de tourner la feuille de $\pi/2$ dans le sens trigonométrique\dots
    \item En notant $F$ la fonction de répartition de la variable aléatoire réelle $X$, on a pour tout $x$ réel,
    \[
      F(x)= \PP{X\le x} = \PP{\tan U \le x} = \PP{U\le \arctan x}.
    \]
    La dernière égalité se justifie comme suit. Rappelons d'abord que la restriction de la fonction tangente à l'intervalle $]-\pi/2,\pi/2[$ est continue strictement croissante avec pour limite à droite $-\infty$ en $-\pi/2$ et pour limite à gauche $+\infty$ en $\pi/2$. Cette restriction réalise donc une bijection\,\footnote{L'injectivité résulte de la croissance stricte et la surjectivité vient de la continuité et des limites aux bornes $\pm \pi/2$ \textit{via} le théorème des valeurs intermédiaires.} de $]-\pi/2,\pi/2[$ sur $\mathbb{R}$. La fonction $\arctan$ est la réciproque de cette bijection. En particulier elle est strictement croissante sur $\mathbb{R}$ et pour tout $u\in]-\pi/2,\pi/2[$, $\arctan(\tan u)=u$, pour tout $x\in\mathbb{R}$, $\tan(\arctan x)=x$. Ceci nous permet de vérifier l'égalité d'événements $\{\tan U\le x\}=\{U\le \arctan x\}$ et donc de leur probabilité. En effet on a les deux implications :
    \begin{align*}
      \tan U \le x \Rightarrow U\le \arctan x,\quad &\text{par croissance d'arc tangente et }\arctan(\tan U)=U,\\
      U \le \arctan x \Rightarrow \tan U\le x,\quad &\text{par croissance de tangente et } \tan(\arctan x)=x.
    \end{align*}
    Ceci justifie l'équivalence $(\tan U\le x) \Leftrightarrow (U\le \arctan x)$ et donc l'égalité des événements correspondants.

    D'autre part, $U$ suivant la loi uniforme sur $]-\pi/2,\pi/2[$, on a en notant $\lambda$ la mesure de Lebesgue sur $\mathbb{R}$ (donc la longueur pour un segment):
    \begin{align*}
      \forall y\in\left]\frac{-\pi}{2},\frac{\pi}{2}\right[,\quad
      \PP{U\le y} & = \PP{U\in]-\infty,y]} = \frac{\lambda(]-\infty,y]\cap]-\pi/2,\pi/2[)}{\lambda(]-\pi/2,\pi/2[)}\\
                & = \frac{\lambda(]-\pi/2,y[)}{\pi} = \frac{y-(-\pi/2)}{\pi}=\frac{1}{2}+\frac{y}{\pi}.
    \end{align*}
    En appliquant ceci à $y=\arctan x$, on aboutit à
    \[
      \forall x\in\mathbb{R},\quad F(x)= \PP{U\le \arctan x}=
      \frac{1}{2}+\frac{1}{\pi}\arctan x.
    \]
    On vérifie facilement qu'il s'agit bien d'une fonction de répartition : elle est croissante et continue sur $\mathbb{R}$ (propriétés héritées d'arc tangente par composition); sa limite en $-\infty$ est $0$, car la limite en $-\infty$ de l'arc tangente est $-\pi/2$, de même sa limite en $+\infty$ est $1$ car celle de l'arc tangente est $\pi/2$.

    \item La fonction $F$ est de classe $C^1$ sur $\mathbb{R}$ comme l'arc tangente. La loi de $X$ admet donc une densité $f$ qui s'obtient par dérivation de $F$:
    \[
      \forall t\in\mathbb{R},\quad
      f(t)
        = F'(t)
        = \frac{\mathrm{d}}{\mathrm{d}t}\left(\frac{1}{2}+\frac{1}{\pi}\arctan t\right)
        = \frac{1}{\pi}\frac{\mathrm{d}}{\mathrm{d}t}(\arctan t)
        = \frac{1}{\pi(1+t^2)}.
    \]
    On reconnaît la densité de la loi de Cauchy.

    \item Pour calculer $\PP{\abs{X}\le 1}$, le plus simple est de remarquer que dans le triangle rectangle $AOM$, $OM\le OA = 1$ si et seulement si l'angle aigu (non orienté) de sommet $A$ a une mesure d'au plus $\pi/4$, c'est-à-dire si et seulement si $\abs{U}\le \pi/4$. D'où
    \[
      \PP{\abs{X}\le 1}=\PP{U\in[-\pi/4,\pi/4]}
      = \frac{\lambda([-\pi/4,\pi/4])}{\lambda(]-\pi/2,\pi/2[)}
      = \frac{1}{2}.
    \]
    Cette méthode avait l'avantage de ne pas utiliser les questions précédentes.

    On pouvait retrouver ce résultat à partir de la fonction de répartition $F$ en notant que:
    \begin{align*}
      \PP{\abs{X}\le 1}
      & = \PP{X\in[-1,1]}\\
      & = F(1)-F(-1^-)
        = F(1)-F(-1)\\
      & = \frac{\arctan(1)-\arctan(-1)}{\pi}
        = \frac{\pi/4-(-\pi/4)}{\pi}=\frac{1}{2}.
    \end{align*}
    On a utilisé la continuité de $F$ pour remplacer la limite à gauche $F(-1-)$ par $F(-1)$.
  \end{enumerate}
\end{solution}


% -----------------------------------
\begin{exo} (Simulation)

  Soit $X$ une variable aléatoire suivant une loi exponentielle de paramètre $a >0$, et dont on note $F$ la fonction de répartition.
  \begin{enumerate}
    \item Expliquer pourquoi on ne peut pas utiliser directement le théorème 3.43 du poly pour simuler la loi de $X$.
    \item Sur quel intervalle maximal $F$ est-elle bijective ? Déterminer sa réciproque, $G$, sur cet intervalle.
    \item Soit $U$ une variable aléatoire suivant une loi uniforme sur $[0,1[$. On pose $Y \coloneqq G(U)$. Quelle est la loi de $Y$ ?
    \item Pour conclure, expliquer pourquoi il suffit et il est intéressant de considérer $-\frac{\ln U}{a}$ pour simuler une variable aléatoire de loi exponentielle de paramètre $a > 0$.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Ici, la fonction de répartition $F$ donnée par
    \[
    F(x)=
      \begin{cases*}
        0          & si $x\leq 0$ \\
        1-e^{- ax} & sinon
      \end{cases*}
    \]
    n'est plus strictement croissante sur $\mathbb{R}$ et donc le théorème 3.43 du poly ne s'applique plus.
    \item L'intervalle maximal sur lequel $F$ est bijective est $[0,+\infty[$. Sur cet intervalle, $F$ est continue et strictement monotone et sa réciproque est donnée par $G(x)=-\frac{1}{a}\ln (1-x)$ pour tout réel $x\in[0,1[$.
    \item Si $U$ suit une loi uniforme sur $[0,1[$ alors $Y \coloneqq G(U)$ (qui est bien définie sur l'ensemble $\{U\in [0,1[\}$ de probabilité $1$)  a pour fonction de répartition $F$. En effet,
    \begin{itemize}
      \item si $x<0$ l'ensemble $\{U\in [0,1[\}\cap \{G(U)\leq x\}=\varnothing$ puisque $G$ est à valeurs dans $[0,+\infty[$ et donc $\PP{Y\leq x}=0$,
      \item si $x\geq 0$, l'ensemble $\{U\in [0,1[\}\cap \{G(U)\leq x\}$ a même probabilité que  $\{U\leq F(x)\}$ et donc, $\PP{Y\leq x}=F(x)$.
    \end{itemize}
    Finalement, $Y$ suit une loi exponentielle de paramètre $a$.
  \end{enumerate}
\end{solution}


% ==================================
\section{Loi normale}
% ==================================


% -----------------------------------
\begin{exo}

  On suppose que les notes d'un contrôle de probabilité suivent une loi normale de paramètre $(8,5;4)$.
  \begin{enumerate}
    \item Quelle est la probabilité pour un étudiant d'avoir la moyenne ?
    \item On veut améliorer les notes à l'aide d'une transformation affine $Y=aX+b$. Déterminer $a$ et $b$ pour qu'un étudiant ait la moyenne avec une probabilité de $1/2$ et une note supérieure à $8$ avec une probabilité de $3/4$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Les trajets dont il est question dans cet exercice sont censés suivre des lois normales et être indépendants.
  \begin{enumerate}
    \item Un employé E quitte son domicile à 8h30. La durée moyenne de son trajet à son lieu de travail est 25 minutes et son écart-type 10 minutes. Quelle est la probabilité qu'il arrive avant 9h sur son lieu de travail ?
    \item Un autre employé, F, doit utiliser consécutivement deux moyens de transport pour se rendre à son travail. Il prend le train à 8h20, et son bus démarre à 8h45. La durée moyenne de son trajet en train est 23 minutes et, d'autre part, la probabilité que ce trajet dure entre 18 et 28 minutes est 0,6915. La durée moyenne de son trajet en bus est 14 minutes et son écart-type est 2 minutes. Quel est l'écart-type du trajet en train ? Quelle est la probabilité que $F$ arrive avant 9h sur son lieu de travail ?
    \item Seuls E et F ont une clé de leur lieu de travail. Quelle est la probabilité que cette agence ouvre avant 9h ?
    \item Même question dans l'hypothèse où ils doivent être présents tous deux pour l'ouverture.
  \end{enumerate}
\end{exo}

% ==================================
\section{Et quand il y a plusieurs variables aléatoires en jeu}
% ==================================


% -----------------------------------
\begin{exo} (Encore un festival de jeux!)

  Un responsable de festival contacte $n$ éditeurs par téléphone. La probabilité que chaque éditeur réponde est $p$, et chaque éditeur est supposé répondre indépendamment des autres.
  \begin{enumerate}
    \item Quelle est la loi de $X_1$, nombre d'éditeurs joints au premier essai ?
    \item Trouver par le calcul la loi de $X_2$, nombre d'éditeurs joints au deuxième essai.
    \item Retrouver ce résultat par un raisonnement direct.
    \item Quelle est la loi de $X_1+X_2$ ?
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item $X_1$ suit une loi binomiale $\mathrm{Bin}(n,p)$.
    \item Pour $0\leq k\leq n$, on a
    \[
      \PP{X_2=k}=\sum_{l=0}^n \PP{X_2=k|X_1=l}P(X_1=l)=\dots=C_n^k (p(1-p))^k(1-p+p^2)^{n-k},
    \]
    ce qui permet de conclure que $X_2$ suit une loi binomiale $\mathrm{Bin}(n,p(1-p))$.
    \item Chaque personne a une probabilité $p(1-p)$ d'être jointe au deuxième essai. En effet, si on note $J_{i,j}$ l'événement \enquote{la $j$-ième personne est jointe au $i$-ième essai} pour $1\leq i\leq 2$ et $1\leq j\leq n$ alors
    \[
      \PP{J_{2,j}}= \PP{J_{2,j}\cap J_{1,j}^c}= \PP{J_{2,j}|J_{1,j}^c}P(J_{1,j}^c)=p(1-p),
    \]
    pour tout $1\leq j\leq n$. Les réponses des $n$ personnes étant indépendantes, on en déduit que la loi de $X_2$ est celle du nombre de succès (ici un succès étant une réponse au deuxième essai) parmi une suite de $n$ épreuves indépendantes ayant deux issues possibles et dont la probabilité de succès est $p(1-p)$: c'est donc une loi binomiale $\mathrm{Bin}(n,p(1-p))$. Remarquez que $X_2$ peut s'écrire sous la forme $X_2=\sum_{j=1}^{n}\epsilon_j$ où les $\epsilon_j= \ind_{J_{2,j}}$ sont des variables aléatoires indépendantes de même loi de Bernoulli de paramètre $p(1-p)$. \item On peut trouver la loi de $X_1+X_2$ par le calcul, comme à la question 2). Ou bien on remarque que $X_1+X_2$ est la variable aléatoire égale au nombre de personnes jointes au premier ou au deuxième essai. Et la probabilité d'être jointe au premier ou au deuxième essai pour une personne donnée est
    \[
      \PP{J_{2,j}\cup J_{1,j}}=p+p(1-p)=p(2-p).
    \]
    Donc, $X_1+X_2$ suit une loi binomiale $\mathrm{Bin}(n,p(2-p))$.

    On peut aussi écrire plus formellement que
    \[
      X_1+X_2=\sum_{j=1}^{n}\left(\ind_{J_{1,j}}+\ind_{J_{2,j}}\right)=\sum_{j=1}^{n}\ind_{J_{1,j}\cup J_{2,j}}
    \]
    et les $\left(\ind_{J_{1,j}\cup J_{2,j}}\right)_{1\leq j\leq n}$ forment une  suite de $n$ variables aléatoires indépendantes de loi de Bernoulli de paramètre $p(2-p)$.
  \end{enumerate}
\end{solution}


% -----------------------------------
\begin{exo} (Mélange de lois [discrètes])

Soient $p \in ]0,1[$ et $\alpha \in \mathbb{R}^+$. Les visiteurs d'un certain festival de jeux choisissent indépendamment l'espace \enquote{jeux d'ambiance} avec la probabilité $p$ et l'espace \enquote{jeux de stratégie} avec la probabilité $1-p$.\\
Soit $X$ le nombre de visiteurs choisissant l'espace \enquote{jeux d'ambiance}.
  \begin{enumerate}
    \item Il est entré 50 visiteurs ce matin. Montrer que $X$ suit une loi binomiale de paramètres 50 et $p$.
    \item On suppose maintenant que le nombre  $N$ de visiteurs est aléatoire et qu'il suit une loi de Poisson de paramètre $\alpha$. Évaluer, pour tout $(k,n) \in \mathbb{N}^2$, $\PP{X=k |N=n}$. Quelle est la loi de $X$ ?
    \item Évaluer, pour tout $(k,n) \in \mathbb{N}^2$, $\PP{N=n |X=k}$.
    \item Montrer que $X$ et $N-X$ sont deux variables aléatoires indépendantes.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}[start=4]
    \item $X$ suit une loi de Poisson de paramètre $\alpha p$, et $N-X$ une loi de Poisson de paramètre $\alpha (1-p)$. On compare ensuite les densités jointes au produit des deux densités.
  \end{enumerate}
\end{solution}


% -----------------------------------
\begin{exo}

  Soient $X$ et $Y$ deux variables aléatoires à valeurs entières, indépendantes, telles que pour tout $k \in \mathbb{N}$, $\PP{X=k}=a(1-a)^k$ et $\PP{Y=k}=b(1-b)^k$, où $a$ et $b$ sont deux éléments de $]0,1]$.\\ Déterminer la loi de la variable aléatoire $Z=\min(X,Y)$.
\end{exo}

\begin{solution}

  Il faut passer par les fonctions de répartition (en fait la fonction de survie est plus simple à manier). On trouve
  \[
    \PP{Z=k}=(1-a)^ k(1-b)^k(a+b-ab).
  \]
\end{solution}


\newpage
% ==================================
\section{Entraînement supplémentaire facultatif}
% ==================================


% -----------------------------------
\begin{exo}

  \begin{enumerate}
    \item Étudier la convergence de l'intégrale généralisée $\displaystyle{\int_0^{+\infty} t \eexp^{-t/2} \dd t}$, et si cette intégrale converge, donner sa valeur.
    \item Soit $X$ une variable aléatoire réelle de densité de probabilité
    \[
      f(x) = \frac{x}{4} \eexp^{-x/2} \ind_{[0, +\infty[}(x).
    \]
    Calculer la fonction de répartition de $X$.
    \item Que vaut $\PP{X \le 0}$ ? Que peut-on en déduire sur $X$ ?
    \item Le graphique ci-après représente le graphe de la fonction $f$. Hachurez d'une couleur la région dont l'aire correspond à $\PP{X \le 4}$, et d'une autre couleur la région dont l'aire correspond à $\PP{X \ge 8}$.
    \begin{center}
      \includegraphics{M54_2018-19_TD2_fig4}\\
      \small{Graphe de la fonction $\displaystyle{f(x)= \frac{x}{4} \eexp^{-x/2} \ind_{[0, +\infty[}(x)}$.}
    \end{center}
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $X$ une variable aléatoire réelle de fonction de répartition $F$ définie sur $\mathbb{R}$ par
  \[
    F(x)=\left(1-\big(1+\frac x2\big)\eexp^{-\frac x2}\right)\ind_{]0,+\infty[}(x).
  \]
  \begin{enumerate}
    \item La loi de $X$ est-elle à densité ? Si oui, déterminer une densité de cette loi.
    \item Que pouvez-vous dire du signe de $X$ ?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $X$ une variable aléatoire réelle de loi uniforme sur $[0,1]$. La variable aléatoire $Y$ est-elle à densité, où $Y$ est définie par :
  \begin{enumerate}
    \item $Y=X^2$ ;
    \item $Y=\sqrt{X}$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo} (Assurances maritimes et concentration du capital)

  Une compagnie d'assurances assure une flotte de 500 navires de pêche valant chacun 1 million d'euros. Le risque assuré est la perte totale du navire qui est un événement de probabilité $\numprint{0,001}$ pour une année.  Les naufrages des différents navires sont considérés comme indépendants. On note $X$ la variable aléatoire égale au nombre de navires perdus en une année.
  \begin{enumerate}
    \item Trouver la loi exacte de $X$.
    \item Évaluer $\PP{X=10}$ en utilisant une approximation de la loi de $X$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo} (Premiers mots et normalité \emph{[issu du partiel 2010]})

  Un chercheur s'intéresse à l'âge moyen auquel les premiers mots du vocabulaire apparaissent chez les jeunes enfants. Il effectue une étude auprès d’un millier de jeunes enfants. Cette étude montre que les premiers mots apparaissent, en moyenne, à 11,5 mois, avec un écart-type de 3,2 mois. Le chercheur décide alors de modéliser cet âge par une variable aléatoire réelle $X$ de loi normale $\mathcal{N}(11,5 ; 3,2)$.
  \begin{enumerate}
    \item Quelle est la probabilité qu'un enfant acquière ses premiers mots avant l'âge de 10 mois ?
    \item Quelle est la probabilité qu'un enfant acquière ses premiers mots après l'âge de 18 mois ?
    \item Afin de détecter d'éventuels problèmes de langage chez un jeune enfant, on décide de définir un seuil d'alerte $s$ (en mois), à partir duquel on considérera qu'il n'est pas \enquote{normal} qu'un enfant n'ait pas acquis ses premiers mots. Le chercheur propose donc de choisir $s$ tel que la probabilité pour qu'un enfant ait acquis ses premiers mots avant $s$ mois soit de $99,8 \%$. Que vaut $s$ ?
    \item La compagnie rembourse le 31 décembre, sur ses réserves, la valeur des bateaux assurés ayant fait naufrage dans l'année. A combien doivent s'élever ces réserves financières pour qu'elle puisse effectuer la totalité de ces remboursements avec une probabilité supérieure à $\numprint{0,999}$?\\
    \begin{indication}
      En utilisant l'approximation poissonnienne de la loi binomiale, montrer qu'il suffit que ces réserves représentent la valeur d'un très petit nombre de navires.
    \end{indication}
    \item La compagnie fusionne avec une autre compagnie identique ($\numprint{500}$ navires assurés). Reprendre brièvement la question 3) et commenter le résultat obtenu.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Une machine-outil produit à la chaîne des objets manufacturés et l'on sait qu'en période de marche normale la probabilité pour qu'un objet soit défectueux est $p$. On se propose de vérifier la machine. À cet effet, on définit la variable aléatoire $T_r$ égale au nombre minimum de prélèvements successifs qu'il faut effectuer pour amener $r$ objets défectueux. Calculer la loi de $T_r$.
\end{exo}


% -----------------------------------
\begin{exo}

  Deux joueurs A et B jouent à pile ou face avec des pièces équilibrées. A lance $(n+1)$ pièces et B $n$ pièces ($n \in \mathbb{N}^\star$). Soient $X$ et $Y$ le nombre aléatoire de faces amenées respectivement par A et B.
  \begin{enumerate}
    \item Calculer la probabilité que $X-Y = k$, pour $k \in \mathbb{Z}$.
    \item Calculer la probabilité que $X=Y$, que $X > Y$.\\
    \begin{indication}
      On pourra
      \begin{itemize}
        \item  soit utiliser la formule suivante (après l'avoir démontrée)
        \[
          \sum_{i=1}^n C_p^iC_q^{n-i}=C_{p+q}^n,
        \]
        \item soit faire intervenir la variable aléatoire $Z=n-Y$.
      \end{itemize}
    \end{indication}
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo} (Mélange de lois)

  On suppose que le nombre $N$ d'œufs pondus par un insecte suit une loi de Poisson de paramètre $\alpha$:
  \[
    \PP{N=k}=\frac{\eexp^{-\alpha}\alpha^k}{k\,!}\qquad k\in {\mathbb{N}}
  \]
  On suppose également que la probabilité de développement d'un œuf est $p$ et que les œufs sont mutuellement indépendants. On note $S$ le nombre (aléatoire) de survivants. Montrer que $S$ suit une loi de Poisson de paramètre $p\alpha$.
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $X$ une variable aléatoire suivant une loi exponentielle de paramètre $a$. On lui associe la variable aléatoire discrète $Y = [X]$, où les crochets désignent la partie entière. Quelle est la loi de $Y$ ? Quelle est la loi de la partie fractionnaire $Z \coloneqq  X-Y$ ?
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $X$ une variable aléatoire de loi uniforme sur $[0,1]$ et $Y$ la variable aléatoire définie sur le même espace $\Omega$ par
  \[
    Y(\omega) \coloneqq
    \begin{cases}
      X(\omega)   &\text{si } X(\omega) \in  [0,1/4] \cup [3/4, 1];   \\
      1-X(\omega) &\text{si } X(\omega) \in\ ]1/4, 3/4[.
    \end{cases}
  \]
  \begin{enumerate}
    \item Quelle est la loi de $Y$ ?
    \item Trouver la fonction de répartition de la variable aléatoire $Z \coloneqq  X+Y$ et vérifier que $Z$ n'est ni discrète ni à densité.
  \end{enumerate}
\end{exo}

\end{document}
