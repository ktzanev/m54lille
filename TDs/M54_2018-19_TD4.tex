\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M54}

\usepackage{mathrsfs}
\usepackage{caption}
  \captionsetup[figure]{aboveskip=-.1\baselineskip,belowskip=.5\baselineskip,font=footnotesize,labelformat=simple,justification=raggedright,width=.91\textwidth}
\usepackage[export]{adjustbox}

% \solutionstrue

\begin{document}

\hautdepage{TD4 - Convergence(s)}

% -----------------------------------
\begin{exo}

  Soit $f$ une densité de probabilité sur $\mathbb{R}$ et soit $X_n$ une suite de variables aléatoires  de densité  $f_n(x)=nf(nx)$. Montrer que les $X_n$ convergent en probabilité vers une variable aléatoire $X$ que l'on déterminera.
\end{exo}


% -----------------------------------
\begin{exo}(Unicité de la limite en probabilité)

  On veut vérifier que la limite en probabilité est unique modulo l'égalité presque-sûre. Pour cela on supposera que $Y_n$ converge en probabilité vers $Y$ \emph{et} vers $Y'$.
  \begin{enumerate}
    \item Justifier pour tout $\varepsilon > 0$ l'inégalité:
     \[
        \PP{\abs{Y-Y'}>\varepsilon}\le
        \PP{\abs{Y-Y_n}>\frac{\varepsilon}{2}} +
        \PP{\abs{Y_n-Y'}>\frac{\varepsilon}{2}}.
    \]
    \item En déduire la valeur de $\PP{\abs{Y-Y'}>\varepsilon}$ et conclure.
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}(Convergence en probabilité et bornitude stochastique)

  Montrez que si $Y_n$ converge en probabilité vers une variable aléatoire $Y$, alors la suite $(Y_n)_{n\geq 1}$ est \emph{stochastiquement bornée}, c'est-à-dire:
  \[
    \forall \varepsilon>0,\;\exists c>0,\;\forall n\ge 1,\quad
    \PP{\abs{Y_n}>c}<\varepsilon.
  \]
\end{exo}


% -----------------------------------
\begin{exo}(Convergence en probabilité et opérations)

  Soient $(X_n)_{n\geq 1}$, $(Y_n)_{n\geq 1}$, $X$ et $Y$ des variables aléatoires définies sur le même espace probabilisé $(\Omega,\mathscr{F},\P)$. On suppose que $X_n$ et $Y_n$ convergent en probabilité vers $X$ et $Y$ respectivement.
  \begin{enumerate}
    \item Montrer que $X_n + Y_n$ converge en probabilité vers $X+Y$.
    \item Montrer que $X_nY_n$ converge en probabilité vers $XY$.\\
    \begin{indication}
      Commencer par montrer que pour tout $\delta>0$, il existe un réel $c$ et un entier $n_0$ tels que pour tout $n\geq n_0$, $\PP{\abs{Y_n}>c}<\delta$.
    \end{indication}
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}(Convergences p.s. ou en probabilité et image continue)

  Soient $(Y_n)_{n\geq 1}$ et $Y$ des variables aléatoires définies
  sur le même espace probabilisé.
  \begin{enumerate}
    \item On suppose que $Y_n$ converge en probabilité vers une constante $c$ et que $g$ est une fonction définie au voisinage de $c$ et continue au point $c$. Montrez que $g(Y_n)$ converge en probabilité vers $g(c)$. On commencera par préciser la définition de $g(Y_n)$.
    \item Même question pour la convergence presque-sûre.
    \item On suppose maintenant que $Y_n$ converge en probabilité vers $Y$ et que $g$ est continue sur $\mathbb{R}$. Montrez que $g(Y_n)$ converge en probabilité vers $g(Y)$.\\
    \begin{indication}
      On pourra commencer par examiner le cas plus simple où $g$ est \emph{uniformément} continue sur $\mathbb{R}$. Pour passer au cas général, on pourra utiliser la bornitude stochastique.
    \end{indication}
    \item Même question pour la convergence presque-sûre.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.7](Convergence en probabilité de gaussiennes)

  Soit $(Y_n)_{n\ge 1}$ une suite de variables aléatoires définies sur le même espace probabilisé, gaussiennes de loi $N(c,\sigma_n)$, où la constante $c$ ne dépend pas de $n$.  Montrez que $Y_n$ converge en probabilité vers $c$ si et seulement si $\sigma_n$ tend vers $0$.
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $p\in]0,1[$ et $(X_i)_{i\in\mathbb{N}^*}$ une suite de variables aléatoires indépendantes et de même loi de Bernoulli de paramètre $p$. On pose, pour $n\in\mathbb{N}^*$,
  \[
    Y_n=X_nX_{n+1}\quad\textrm{et}\quad U_n=Y_1+\dots+Y_n.
  \]
  \begin{enumerate}
    \item Quelle est la loi de $Y_n$ ($n\in\mathbb{N}^*$)?
    \item Pour quels couples $(n,m)$ les variables $Y_n$ et $Y_m$ sont-elles indépendantes ?
    \item Calculer l'espérance et la variance de $U_n$ ($n\in\mathbb{N}^*$).
    \item Montrer que pour tout $\varepsilon >0$,
    \[
      \lim_{n\to +\infty}\PP{\Bigl|\frac{U_n}{n}-p^2\Bigr|\geq\varepsilon}=0.
    \]
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}(Hasard et compensations exactes)

  On considère une épreuve ayant $r$ issues élémentaires équiprobables ($r=2$ pour le lancer d'une pièce,  $r=6$ pour celui d'un dé, \dots). On répète cette épreuve dans des conditions identiques. On note $A_n$ l'événement: \emph{au cours des $nr$ premières épreuves, chacune des $r$ issues distinctes se produit exactement $n$ fois}. On dira que $A_n$ est une \emph{compensation exacte}.
  \begin{enumerate}
    \item Calculer $p_n=\PP{A_n}$.
    \item Donner un équivalent de $p_n$ quand $n$ tend vers l'infini en utilisant la formule de Stirling.
    \item En déduire que si $r\geq 4$, presque sûrement il n'y aura plus jamais de compensation exacte au delà d'un certain nombre d'épreuves.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.49](Une armée de singes dactylographes peut elle taper par
hasard  \emph{Don Quichotte)?}

  Montrer que dans le jeu de pile ou face infini, la séquence \texttt{pfffp} apparaît presque sûrement une infinité de fois. Généraliser.
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(X_k)_{k\ge 1}$ une suite de variables aléatoires définies sur le même espace probabilisé, indépendantes et de même loi. Montrez que
  \[
    Y_n=\frac{1}{n}\sum_{k=1}^n \frac{X_k}{1+\abs{X_k}}
  \]
  converge presque-sûrement quand $n$ tend vers l'infini vers une constante $c\in[-1,1]$.
\end{exo}


% -----------------------------------
\begin{exo}(Escargot aléatoire)

\sidebyside{.5}{
  Soit $(X_i)_{i\in \mathbb{N}^{\ast}}$ une suite de variables aléatoires, définies sur le même espace probabilisé $(\Omega,\mathscr{F},\P)$, indépendantes et de même loi, de carré intégrable ($\E X_1^2 <+\infty$). On définit par récurrence la suite de variables aléatoires positives $(R_n)_{n\geq 1}$ par
  \[
    R_1=\abs{X_1},\quad R_n=\sqrt{R_{n-1}^2 +X_n^2},\quad n\geq 2.
  \]
}{
  \vspace{-14mm}
  \includegraphics[width=7cm,valign=t]{M54_2018-19_TD4_escargot1}
  \captionof{figure}{Construction de l'escargot aléatoire $\mathscr{E}_7$}
  \label{Fig-constr-escrg}
}

  \begin{enumerate}
  \item Montrer que la suite $(n^{-1/2}R_n)_{n\geq 1}$ converge presque sûrement vers une limite constante $\tau$ que l'on exprimera en fonction de $\E X_1^2$.
  \item
  \sidebyside{.7}{Pour illustrer graphiquement cette convergence, on prend les $X_i$ de même loi uniforme sur $[0,1]$ et on construit dans un repère orthonormé du plan la suite $(M_n)_{n\geq 1}$ de points aléatoires par la récurrence suivante. On pose d'abord $M_1=(X_1,0)$, puis connaissant $M_{n-1}$ on obtient $M_n$ comme l'unique point tel que $M_{n-1}M_n=X_n$ et que l'angle $(\overrightarrow{M_{n-1}M_n\,},\overrightarrow{M_{n-1}O\,})$ ait pour mesure $+\pi/2$. En d'autre termes, on construit $M_n$ tel que le triangle $OM_{n-1}M_n$ soit rectangle en $M_{n-1}$, de côtés de l'angle droit $OM_{n-1}=R_{n-1}$ et $M_{n-1}M_n=X_n$ et en tournant toujours dans le sens trigonométrique. On trace ainsi la ligne polygonale $\mathscr{E}_n$ de sommets $M_1,M_2,\dots,M_n$ (l'escargot aléatoire, cf. figure~\ref{Fig-constr-escrg}).}{
    \captionsetup[figure]{width=.7\textwidth}
    \centering
    \includegraphics[width=4.2cm,valign=t]{M54_2018-19_TD4_escargot2}
    \captionof{figure}{Escargot aléatoire $\mathscr{E}_{200}$ (simulation)}
    \label{Fig-simul-escrg}
  }

  On fixe un $\varepsilon>0$. Montrer que presque sûrement, $\mathscr{E}_n$ est inclus dans le disque de centre $O$ et de rayon $(1+\varepsilon)\sqrt{n/3}$ pour tout $n$ assez grand (cf. figure~\ref{Fig-simul-escrg}).
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}(Borel-Cantelli et les retours à l'origine)

  Soit $(X_n)$ une suite de v.a.r. indépendantes telles que:
  \[
    \PP{X_i=+1}=p, \quad \PP{X_i=-1}=1-p=q
      \quad\text{avec}\quad
    0<p<1,\quad p\neq \frac{1}{2}.
  \]
  On pose:
  \[
    S_n=\sum_{i=1}^{n} X_i
      \quad\text{et}\quad
    A_n=\{S_n=0\}.
  \]
  L'événement $A_n$ est un retour à zéro. On pose
  \[
    A:=\ensemble{\omega\in\Omega}{\text{la suite $S_n(\omega)$ repasse une infinité de fois en $0$}}
    =
    \bigcap_{k\geq 1}\bigcup_{j\geq k}A_j.
  \]
  \begin{enumerate}
  \item Prouver que $\PP{A}=0$.
  \item En utilisant la loi forte des grands nombres, donner une conclusion plus précise permettant de retrouver le résultat précédent.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}(Loi de Cauchy et LFGN)

  Le but de cet exercice est d'étudier la convergence des moyennes arithmétiques d'une suite de v.a. indépendantes et de même loi de Cauchy. Toutes les variables aléatoires intervenant dans l'énoncé sont supposées définies sur le même espace probabilisé $(\Omega,\mathscr{F},\P)$.
  \begin{enumerate}
    \item Soit $X$ une variable aléatoire réelle suivant la loi de Cauchy de densité:
    \[
      f:\mathbb{R}\to\mathbb{R}_+,\quad t\mapsto \frac{1}{\pi(1+t^2)}.
    \]
    Pour tout entier $n\ge 1$, exprimez $\PP{X>n}$ à l'aide d'une intégrale. Déduisez-en la minoration:
    \[
      \forall n\ge 1,\quad \PP{X>n}\ge \frac{1}{2\pi n}.
    \]
    \item Soit $(X_k)_{k\ge 1}$ une suite de variables aléatoires indépendantes et de même loi que $X$. On pose
    \[
      A:=\ensemble{\omega\in\Omega}{X_k(\omega)>k\text{ pour une infinité d'indices $k$}}.
    \]
    Expliquez pourquoi $\PP{A}=1$.
    \item Déduire de ce qui précède que la suite $(X_k)_{k\ge 1}$ ne vérifie pas la loi forte des grands nombres.\\
    \begin{indication}
      En posant comme d'habitude $S_n:=X_1+\dots+ X_n$, exprimez $\frac{X_n}{n}$ en fonction de $\frac{S_n}{n}$ et $\frac{S_{n-1}}{n-1}$ et raisonnez par l'absurde en supposant que $\frac{S_n}{n}$ converge p.s. vers une certaine variable aléatoire $Y$ (pas forcément constante).
    \end{indication}
    \item Ce résultat est-il contradictoire avec la loi forte des grands nombres ?
  \end{enumerate}

\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(X_k)_{k\geq 1}$ une suite de variables aléatoires définies sur le même espace probabilisé $(\Omega,\mathscr{F},\P)$, d'espérance nulle et vérifiant:
  \[
    \forall i,j\in\mathbb{N}^\ast,
      \quad
    \E(X_iX_j)=
      \begin{cases}
        1 & \text{si $i=j$,}\\
        0 & \text{si $i\neq j$.}
      \end{cases}
  \]
  On pose $S_n:=X_1 + \dots + X_n$. Montrer que pour tout $\alpha>1$,
  \[
    \frac{S_n}{n^\alpha}\xrightarrow[n\to + \infty]{\text{p.s.}} 0.
  \]
  \emph{Nota bene.} Vous avez bien lu, il n'y a pas d'erreur dans l'énoncé, on ne suppose pas les $X_k$ indépendantes.
\end{exo}


\end{document}



