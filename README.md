# [m54lille](https://gitlab.com/ktzanev/m54lille) / [page web](https://ktzanev.gitlab.io/m54lille)

Les tds du module M54 - «Probabilité» de l'Université de Lille

## 2018/19

Vous pouvez récupérer [ce dépôt](https://gitlab.com/ktzanev/m54lille) de deux façons faciles :

- en téléchargeant l'archive [zip](https://gitlab.com/ktzanev/m54lille/-/archive/master/m54lille-master.zip) qui contient la dernière version des fichiers,
- récupérer l'intégralité de ce dépôt, y compris l'historique des modifications, en utilisant `git` avec la commande

  ~~~~~~~
  git clone git@gitlab.com:ktzanev/m54lille.git
  ~~~~~~~

Dans [ce dépôt](https://gitlab.com/ktzanev/m54lille) vous pouvez trouver les sources LaTeX et les PDFs (compilés avec XeLaTeX) suivants :

- Feuille de TD n°1
  [[tex](https://ktzanev.gitlab.io/m54lille/TDs/M54_2018-19_TD1.tex)]
  [[pdf](https://ktzanev.gitlab.io/m54lille/TDs/M54_2018-19_TD1.pdf)]
  ou
  [[pdf@latexonline.cc](http://latexonline.cc/compile?git=https://gitlab.com/ktzanev/m54lille.git&command=xelatex&target=TDs/M54_2018-19_TD1.tex)]
- Feuille de TD n°2
  [[tex](https://ktzanev.gitlab.io/m54lille/TDs/M54_2018-19_TD2.tex)]
  [[pdf](https://ktzanev.gitlab.io/m54lille/TDs/M54_2018-19_TD2.pdf)]
  ou
  [[pdf@latexonline.cc](http://latexonline.cc/compile?git=https://gitlab.com/ktzanev/m54lille.git&command=xelatex&target=TDs/M54_2018-19_TD2.tex)]
- Feuille de TD n°3
  [[tex](https://ktzanev.gitlab.io/m54lille/TDs/M54_2018-19_TD3.tex)]
  [[pdf](https://ktzanev.gitlab.io/m54lille/TDs/M54_2018-19_TD3.pdf)]
  ou
  [[pdf@latexonline.cc](http://latexonline.cc/compile?git=https://gitlab.com/ktzanev/m54lille.git&command=xelatex&target=TDs/M54_2018-19_TD3.tex)]
- Feuille de TD n°4
  [[tex](https://ktzanev.gitlab.io/m54lille/TDs/M54_2018-19_TD4.tex)]
  [[pdf](https://ktzanev.gitlab.io/m54lille/TDs/M54_2018-19_TD4.pdf)]
  ou
  [[pdf@latexonline.cc](http://latexonline.cc/compile?git=https://gitlab.com/ktzanev/m54lille.git&command=xelatex&target=TDs/M54_2018-19_TD4.tex)]
- Feuille de TD n°5
  [[tex](https://ktzanev.gitlab.io/m54lille/TDs/M54_2018-19_TD5.tex)]
  [[pdf](https://ktzanev.gitlab.io/m54lille/TDs/M54_2018-19_TD5.pdf)]
  ou
  [[pdf@latexonline.cc](http://latexonline.cc/compile?git=https://gitlab.com/ktzanev/m54lille.git&command=xelatex&target=TDs/M54_2018-19_TD5.tex)]

---

- Sujet et solutions du contrôle de groupe 1
  [[tex](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_CC_G1.tex)]
  [[sujet pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_CC_G1.pdf)]
  et
  [[solutions pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_CC_G1_solutions.pdf)]
- Sujet et solutions du contrôle de groupe 2
  [[tex](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_CC_G2.tex)]
  [[sujet pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_CC_G2.pdf)]
  et
  [[solutions pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_CC_G2_solutions.pdf)]
- Sujet et solutions du contrôle de groupe 3
  [[tex](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_CC_G3.tex)]
  [[sujet pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_CC_G3.pdf)]
  et
  [[solutions pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_CC_G3_solutions.pdf)]
- Sujet et solutions du DS1
  [[tex](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_DS1.tex)]
  [[sujet pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_DS1_sujet.pdf)]
  et
  [[solutions pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_DS1_solutions.pdf)]
- Sujet et solutions du DS2
  [[tex](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_DS2.tex)]
  [[sujet pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_DS2_sujet.pdf)]
  et
  [[solutions pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_DS2_solutions.pdf)]
- Sujet et solutions du rattrapage
  [[tex](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_Rattrapage.tex)]
  [[sujet pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_Rattrapage_sujet.pdf)]
  et
  [[solutions pdf](https://ktzanev.gitlab.io/m54lille/Examens/M54_2018-19_Rattrapage_solutions.pdf)]


Pour compiler ces feuilles de td vous avez besoin de la feuille de style [M54.sty](https://ktzanev.gitlab.io/m54lille/TDs/M54.sty) ainsi que le [logo du département](https://ktzanev.gitlab.io/m54lille/TDs/ul-fst-math_noir.pdf).

## Historique

Ces feuilles sont basées sur [les feuilles](http://math.univ-lille1.fr/~debievre/m54_2017.html) de [S. De Bièvre](http://math.univ-lille1.fr/~debievre) ainsi que sur [celles du module M66](http://math.univ-lille1.fr/~suquet/enseignement.html#Anncr) de [C. Suquet](http://math.univ-lille1.fr/~suquet/)
