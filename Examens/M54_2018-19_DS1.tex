\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M54}

% pour voir les solutions il faut enlever le commentaire de la ligne suivante
\solutionstrue

\begin{document}


% ==================================
\hautdepage[-1]{
  \sisujet{Devoir surveillé}\sisolutions{Solutions du devoir surveillé}\\
  \normalfont\normalsize
  7 novembre 2018\\
  {[~14h00--16h00~]}
}
% ==================================
\vspace{-2mm}

\sisujet{
  \attention~\textbf{Aucun document n'est autorisé.}
  \vspace{5mm}
  \tsvp
}


%-----------------------------------
\begin{exo} (Fonction de répartition et densité)

  Soient $\alpha >0$ et $\lambda >0$ deux réels strictement positifs, et soit $f_{\alpha,\lambda}$ la fonction définie sur $\mathbb{R}_+$ par :
  $$
    f_{\alpha,\lambda}(x) =  \frac{\alpha}{\lambda} \left(\frac{x}{\lambda}\right)^{\alpha-1} e^{-(\frac{x}{\lambda})^\alpha} \ind_{[0,+\infty[},
  $$
  où $\ind_{[0,+\infty[}$ est la fonction indicatrice de $[0,+\infty[$.
  \begin{enumerate}
    \item Montrer que $f_{\alpha,\lambda}$ définit bien une densité de probabilité.
  \end{enumerate}
  Soit $X$ une variable aléatoire dont la densité de probabilité est $f_{\alpha,\lambda}$.
  \begin{enumerate}[resume]
    \item Calculer $\mathbb{P}(X = 1)$
    \item Quelle est la fonction de répartition de la variable $X$ ?
    \item Soi $Y = X^{\alpha}$. Quelle est la loi de $Y$ ?
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Comme $\alpha$ et $\lambda$ sont positifs, la fonction $f_{\alpha,\lambda}$ est positive sur $\mathbb{R}_+$. La fonction $f_{\alpha,\lambda}$ est bien intégrable sur $\mathbb{R}$, car $\alpha > 0$, donc $1-\alpha < 1$ et la fonction $x \mapsto x^{\alpha-1}$ est donc bien intégrable entre 0 et 1. Enfin, on a :
    \begin{align*}
      \int_{\mathbb{R}} f_{\alpha,\lambda}(x)dx & = \int_0^{+\infty} \frac{\alpha}{\lambda} \left(\frac{x}{\lambda}\right)^{\alpha-1} e^{-(\frac{x}{\lambda})^\alpha} dx \\
          & = \left[- e^{-(\frac{x}{\lambda})^\alpha }\right]_0^{+\infty}  \\
          & = 1.
    \end{align*}
    La fonction $f_{\alpha,\lambda}$ vérifie donc bien les propriétés d'une densité de probabilité.
    \item Comme $X$ admet une densité, sa loi vérifie $\PP{X=a} = 0$ pour tout $a \in \mathbb{R}$. En particulier, $\PP{X=1} = 0$.
    \item Notons $F_{\alpha,\lambda}$ la fonction de répartition de $X$. Comme $X$ est à valeurs positives, on a $F_{\alpha,\lambda}(x) = 0$ pour tout $x < 0$. Puis, par définition de la fonction de répartition, on a, pour tout $x \geq 0$ :
    \begin{align*}
      F_{\alpha,\lambda} (x)
        & = \int_{-]\infty,x]} f_{\alpha,\lambda} (t) dt \\
        & = \int_0^x \frac{\alpha}{\lambda} \left(\frac{t}{\lambda}\right)^{\alpha-1} e^{-(\frac{t}{\lambda})^\alpha} dt \\
        & = \left[- e^{-(\frac{t}{\lambda})^\alpha }\right]_0^{x}  \\
        & = 1 - e^{-(\frac{x}{\lambda})^\alpha}.
    \end{align*}
    Finalement, on a donc $F_{\alpha,\lambda} (x) =  (1 - e^{-(\frac{x}{\lambda})^\alpha}) \ind_{[0,+\infty[}$.
    \item Soit $F_Y$ la fonction de répartition de $Y$. La variable aléatoire $Y$ est positive, donc $F_Y(y) = 0$ pour tout $y < 0$. Puis, pour $y \geq 0$, on a :
    \begin{align*}
      F_Y(y) & = \PP{Y \leq y} \\
             & = \PP{X^\alpha \leq y} \\
             & = \PP{X \leq y^{1/\alpha}} \\
             & = 1 - \exp \left(-\left(\frac{y^{1/\alpha}}{\lambda}\right)^\alpha\right) \\
             & = 1 - \exp \left(-\frac{1}{\lambda^\alpha} \ y\right).
    \end{align*}
    D'où, $F_Y(y) = \left(1 - \exp (-y / \lambda^\alpha) \right)\ind_{[0,+\infty[}.$ On reconnaît la loi exponentielle de paramètre $1/\lambda^\alpha$.
  \end{enumerate}
\end{solution}


%-----------------------------------
\begin{exo}(Dénombrement)

  Sylvia, Patricia et Barbara jouent au Bingo. Dans ce jeu, il s'agit de choisir un nombre $n\le 20$ de numéros parmi $\{1,\ldots, 80\}$. Albert le croupier tire ensuite 20 numéros au hasard. On remporte le jackpot en criant \enquote{Bingo} si tous les $n$ numéros sont tirés, et on ramène la cuillère en bois si aucun numéro n'est tiré.  On note $p_{n,i}$ la probabilité d'avoir $i\le n$ numéros gagnants. On note $p_n = p_{n,n}$ la probabilité de gagner le jackpot et $q_n = p_{n,0}$ celle de ramener la cuillère en bois.

  \begin{enumerate}
    \item Calculer $p_n$ et $q_n$ pour tous $n\in\{1,\ldots, 20\}$ et montrer qu'on a toujours $p_n < q_n.$
    \item Montrer que $n\mapsto p_n$ et $n\mapsto q_n$ sont des fonctions strictement décroissantes sur $\{1,\ldots, 20\}.$
    \item Calculer $p_{n,i}$ pour tous $i\le n.$
    \item Montrer que
    $$
      i\mapsto\frac{p_{n,i+1}}{p_{n,i}}
    $$
    est strictement décroissante sur $\{0,\ldots,n-1\}.$ En déduire qu'il existe au plus deux indices $i$ tels que $p_{n,i}$ soit maximale. Déterminer ces indices maximaux pour $n=5$ (choix de Sylvia), $n=10$ (choix de Patricia) et $n=15$ (choix de Barbara).
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Par équiprobabilité, on a
    $$
      p_n \, =\, \frac{\binom{20}{n}}{\binom{80}{n}}\, =\, \frac{20.19\ldots (20-n+1)}{80.79\ldots (80-n+1)}
    $$
    et
    $$
      q_n \, =\, \frac{\binom{60}{n}}{\binom{80}{n}}\, =\, \frac{60.59\ldots (60-n+1)}{80.79\ldots (80-n+1)}\cdot
    $$
    Il est clair que $p_n < q_n.$
    \item Evident par les formules de la question précédente car on multiplie par des nombres dans $]0,1[.$
    \item On a
    $$
      p_{n,i} \, =\, \frac{\binom{20}{i}\binom{60}{n-i}}{\binom{80}{n}}\cdot
    $$

    \item On calcule
    $$
      \frac{p_{n,i+1}}{p_{n,i}}\; =\; \frac{(n-i)(20-i)}{(i+1)(61+i-n)}
    $$
    qui est une fonction strictement décroissante de $i$ et prend la valeur 1 au plus une fois. Si elle ne prend pas la valeur 1 alors un seul indice $i$ maximise $p_{n,i}.$ Si elle prend la valeur 1 alors deux indices $i$ maximisent $p_{n,i}.$ On calcule ces valeurs en résolvant $(n-x)(20-x) = (x+1)(61+x -n),$ soit
    $$
      x\; =\; \frac{21 n-61}{42}\cdot
    $$
    On prend ensuite le seuil de $x.$
    \begin{itemize}
      \item Pour $n = 5$ on a $x = 22/21\in ]1,2[$ et donc $i_{{\rm max}} = 2.$
      \item Pour $n = 10$ on a $x = 149/42 \in ]3,4[$ et donc $i_{{\rm max}} =4.$
      \item Pour $n = 15$ on a $x = 127/21 \in ]6,7[$ et donc $i_{{\rm max}} = 7.$
    \end{itemize}
  \end{enumerate}
\end{solution}

%-----------------------------------
\begin{exo} (Probabilités conditionnelles et indépendance)

  Soient $X$ et $Y$ deux variables aléatoires à valeurs dans $\mathbb{N}$ telles que $\PP{X\le Y} = 1.$ On suppose que la loi de $X$ sachant $Y$ est uniforme, soit
  $$
    \PP{X= i\, \vert\, Y=n}\; =\; \frac{1}{n+1}
  $$
  pour tout $n\ge 0$ et $i\in\{0,\ldots, n\}.$

  \begin{enumerate}
    \item Montrer que la quantité $\PP{\{X=i\}\cap\{Y-X = j\}}$ ne dépend que de $i+j.$ En déduire que
    $$
      \PP{\{X=i\}\cap\{Y-X = j\}}\; =\; \PP{\{X=j\}\cap\{Y-X = i\}}
    $$
    pour tous $i,j\ge 0.$
    \item En déduire que $X$ et $Y-X$ ont la même loi, autrement dit
    $$
      \PP{X= i} \; = \; \PP{Y-X = i}
    $$
    pour tout $i\ge 0.$
  \end{enumerate}
    On suppose de plus que $X+1$ suit une loi géométrique de paramètre $p\in]0,1[,$ c'est à dire
    $$
      \PP{X=i} \; =\; p(1-p)^i
    $$
    pour tout $i \ge 0.$
  \begin{enumerate}[resume]
    \item Montrer que
    $$
      \PP{X=i}\; =\; \sum_{j\ge 0}\; \PP{\{X=i\}\cap\{Y-X = j\}}\; =\;\sum_{k\ge i}\left( \frac{\PP{Y = k}}{k+1}\right)
    $$
    et en déduire que
    $$
      \frac{\PP{Y = k}}{k+1}\; =\; p^2(1-p)^k.
    $$
    \item En déduire que $X$ et $Y-X$ sont indépendantes, soit
    $$
      \PP{\{X=i\}\cap\{Y-X = j\}}\; =\; \PP{X=i}\,\times\,\PP{Y-X = j}
    $$
    pour tous $i, j\ge 0$.
  \end{enumerate}

  \medskip

  On considère maintenant $U$ et $V$ deux variables aléatoires indépendantes et de même loi géométrique de paramètre $p\in]0,1[.$ On pose $X = U-1, Y = V-1$ et $Z = X+Y.$

  \begin{enumerate}[resume]
    \item Calculer $\PP{Z=i}$ pour tout $i\ge 0$.
    \item En déduire que la loi de $X$ sachant $Z$ est uniforme sur $\{0,\ldots, Z\},$ autrement dit
    $$
      \PP{X= i\, \vert\, Z=n}\; =\; \frac{1}{n+1}
    $$
    pour tout $n\ge 0$ et $i\in\{0,\ldots, n\}.$
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item On a
    \begin{align*}
      \PP{\{X=i\}\cap\{Y-X = j\}}
        & = \PP{\{X=i\}\cap\{Y= i+j\}}\\
        & = \PP{X=i\,\vert\, Y= i+j}\PP{Y=i+j}\; =\;\frac{\PP{Y=i+j}}{i+j+1}
    \end{align*}
    qui ne dépend que de $i+j$. On peut alors intervertir $i$ et $j$ et il vient
    $$
      \PP{\{X=i\}\cap\{Y-X = j\}}\; =\; \PP{\{X=j\}\cap\{Y-X = i\}}
    $$
    pour tous $i,j\ge 0.$
    \item On somme l'identité précédente en $j$ pour trouver
    \begin{align*}
      \PP{X=i}
        &= \sum_{j\ge 0}\;\PP{\{X=i\}\cap\{Y-X = j\}}\\
        &= \sum_{j\ge 0}\;\PP{\{X=j\}\cap\{Y-X = i\}} = \PP{Y-X =i}
    \end{align*}
    pour tout $i\ge 0,$ comme demandé.
    \item On a
    \begin{align*}
      \PP{X=i}
        &= \sum_{j\ge 0}\; \PP{\{X=i\}\cap\{Y= i+j\}}\\
        &= \sum_{j\ge 0}\; \frac{\PP{Y=i+j}}{i+j+1} = \sum_{k\ge i}\left( \frac{\PP{Y = k}}{k+1}\right)
    \end{align*}
    en posant $k =i+j$ pour la dernière égalité. On a alors
    $$
      \frac{\PP{Y = k}}{k+1}\; =\; \PP{X=k} \, -\, \PP{X=k+1}\; =\; p^2(1-p)^k.
    $$
    \item D'après la question précédente
    \begin{align*}
      \PP{\{X=i\}\cap\{Y-X = j\}}
        &= \frac{\PP{Y=i+j}}{i+j+1} = p^2(1-p)^{i+j}\\
        &= p(1-p)^i\, \times\, p(1-p)^j = \PP{X=i}\,\times\,\PP{Y-X = j}
    \end{align*}
    puisque $\PP{X=i} =\PP{Y-X =i} = p(1-p)^i.$ Donc $X$ et $Y-X$ sont indépendantes.
  \end{enumerate}

  On considère maintenant $U$ et $V$ deux variables aléatoires indépendantes et de même loi géométrique de paramètre $p\in]0,1[.$ On pose $X = U-1$, $Y = V-1$ et $Z = X+Y$.

  \emph{Il ne faut pas confondre ces $X$ et $Y$ avec les $X$ et $Y$ des questions précédentes.}
  \begin{enumerate}[resume]
    \item Par l'indépendance de $U$ et $V$, et donc de $X$ et $Y$, on a
    $$
      \PP{Z=i}\; =\; \sum_{k=0}^i \PP{X=k}\PP{Y=i-k}\; =\; (i+1)p^2(1-p)^i
    $$
    pour tout $i\ge 0.$
    \item D'après la question précédente
    $$
      \PP{X=i\,\vert\, Z=n}\; =\; \frac{\PP{\{X=i\}\cap\{Y=n-i\}}}{\PP{Z=n}}\; =\; \frac{1}{n+1}
    $$
    comme requis.
  \end{enumerate}
\end{solution}

%-----------------------------------
\begin{exo} (Gémétrie aléatoire)

  \sidebyside{.59}{
    Soient $\mathcal{C}=\mathcal{C}(O,R)$ le cercle de centre $O$ et de rayon $R$ du plan euclidien, et $M \in \mathcal{C}$ un point de ce cercle. On choisi au hasard un point $A$ de $\mathcal{C}$, de façon uniforme.
  }{
    \vspace{-14mm}
    \includegraphics{M54_2018-19_DS1_fig1}
    \vspace{-7mm}
  }
  \begin{enumerate}
    \item Soit $A$ l'événement «$|AM| > R$». Calculer $\PP{A}$.
    \item Soit $B$ l'événement «$|\langle \vv{OM}, \vv{OA} \rangle| > \frac{R^2}{2}$». Calculer $\PP{B}$.
    \indic{Ici $\langle \cdot, \cdot\rangle$ designe le produit scalaire.}
    \item Est-ce que les événements $A$ et $B$ sont indépendants ?
  \end{enumerate}
\end{exo}

\begin{solution}

  Choisir $A$ au hasard de façon uniforme sur $\mathcal{C}$ revient au même de choisir uniformément l'angle $\widehat{MOA}$ dans $[0,2\pi[$.
  \begin{enumerate}
    \item \sidebyside{.59}{
      Le cas $|AM|=R$ est le cas où le triangle $MOA$ est equilateral, c'est à dire quand $\widehat{MOA}=\frac{\pi}{3}$ ou $\widehat{MOA}=\frac{5\pi}{3}$. Ainsi $\PP{|MA| > R} = \frac{\frac{5\pi}{3}-\frac{\pi}{3}}{2\pi-0} = \frac{2}{3}$.
    }{
      \vspace{-14mm}
      \includegraphics{M54_2018-19_DS1_fig2}
      \vspace{-7mm}
    }
    \item \sidebyside{.7}{
      Comme $|\langle \vv{OM}, \vv{OA} \rangle| = R^2\cos(\widehat{MOA})$, la condition $|\langle \vv{OM}, \vv{OA} \rangle| > \frac{R^2}{2}$ est équivalente à $|\cos(\widehat{MOA})| > \frac{1}{2} \iff \widehat{MOA} \in [0,\frac{\pi}{3}[ \cup ]\frac{2\pi}{3}, \frac{4\pi}{3}[ \cup ]\frac{5\pi}{3}, 2\pi[.$ Donc $\PP{|\langle \vv{OM}, \vv{OA} \rangle| > \frac{R^2}{2}} = \frac{2}{3}$.
    }{
      \vspace{-14mm}
      \hfill\includegraphics{M54_2018-19_DS1_fig3}
      \vspace{-7mm}
    }
    \item \sidebyside{.49}{
      D'après les questions précédentes $A \cap B \iff \widehat{MOA} \in ]\frac{2\pi}{3}, \frac{4\pi}{3}[$. Et donc $\PP{A \cap B} = \frac{1}{3} \neq \frac{2}{3}\frac{2}{3} = \PP{A}\PP{B}$. Ainsi $A$ et $B$ sont dépendants.
    }{
      \vspace{-14mm}
      \includegraphics{M54_2018-19_DS1_fig4}
      \vspace{-7mm}
    }
  \end{enumerate}
\end{solution}


\end{document}
