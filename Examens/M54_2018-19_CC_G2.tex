\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M54}

% pour voir les solutions il faut enlever le commentaire de la ligne suivante
% \solutionstrue

\DeclareMathOperator{\atanh}{atanh}


\begin{document}

% ==================================
\hautdepage[-1]{
  \sisujet{Interrogation}\sisolutions{Solutions de l'interrogation}\\
  \normalfont\normalsize
  18 octobre 2018\\
  {[ durée: 1 heure ]}
}
% ==================================
\sisujet{
  \attention~\textbf{Aucun document n'est autorisé.}
  \vspace{7mm}
}

%-----------------------------------
\begin{exo}(Tribus)

  \begin{enumerate}
    \item Donner l'exemple de deux tribus non triviales\footnote{Distinctes de $\{\emptyset,\mathbb{Z}\}$ et de $\mathcal{P}\left(\mathbb{Z}\right)$.} de $\mathbb{Z}$ distinctes.
    \item Déterminer un système exhaustif d'événements\footnote{C'est-à-dire une partition de $\mathbb{Z}$.} qui engendre la même tribu de $\mathbb{Z}$ que les deux parties $2\mathbb{Z}$ et $4\mathbb{Z}$. Combien d'éléments possède cette tribu?
    \item Soit $\mathcal{T}$ la tribu de $\mathbb{Z}$ engendrée par les ensembles $A_n=\{n,n+1\}$ pour $n \in \mathbb{Z}$. Quels sont les éléments de la tribu $\mathcal{T}$?
  \end{enumerate}

\end{exo}

\begin{solution}
  \begin{enumerate}
    \item On sait que la tribu engendrée par un événement $A \subset \Omega$ est $\mathcal{F}_A= \{\emptyset,A,A^c,\Omega\}$. Ainsi les tribus $\mathcal{F}_{\{1\}} = \bigl\{\emptyset,\{1\},\mathbb{Z}\setminus\{1\},\mathbb{Z}\bigr\}$ et $\mathcal{F}_{\{0\}} = \bigl\{\emptyset,\{0\},\mathbb{Z}^*,\mathbb{Z}\bigr\}$ sont clairement non triviales et distinctes.
    \item La tribu engendrée par deux parties $A \subset B$ est la tribu engendrée par le système exhaustif $\{A,B\setminus A, B^c\}$. Donc la tribu engendrée par $4\mathbb{Z}\subset 2\mathbb{Z}$ est la tribu engendrée par le système exhaustif $\{4\mathbb{Z}, 4\mathbb{Z}+2,2\mathbb{Z}+1\}$, car $2\mathbb{Z} \setminus 4\mathbb{Z} = 4\mathbb{Z}+2$ et $(2\mathbb{Z})^{c} = 2\mathbb{Z}+1$. Donc c'est une tribu à $2^{3}=8$ éléments.
    \item Comme $A_{n-1} \cap A_{n} = \{n\} \in \mathcal{T}$ pour tout $n \in \mathbb{Z}$, alors toutes les réunions dénombrables des $\{n\}$ sont des éléments de $\mathcal{T}$, donc $\mathcal{T} = \mathcal{P}(\mathbb{Z})$, l'ensemble de toutes les parties de $\mathbb{Z}$.
  \end{enumerate}
\end{solution}

%-----------------------------------
\begin{exo}(Indépendance)

  On se place dans $\Omega = [0,1]$ muni de la tribu borélienne et de la mesure uniforme.
  \begin{enumerate}
    \item Soient $A$ et $B$ deux événements indépendants dont la réunion est $\Omega$ et qui ont la même probabilité $p$. Quelles valeurs peut prendre $p$ ?
    \item Donner l'exemple de deux tels événements distincts $A$ et $B$.
  \end{enumerate}

\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Comme $A$ et $B$ sont indépendants, $\PP{A \cap B}=\PP{A}\PP{B}=p^{2}$. Et comme $\Omega = A \cup B$, nous avons $1 = \PP{A \cup B} = \PP{A}+\PP{B}-\PP{A \cap B} = p+p-p^{2}$. Donc $(1-p)^{2}=0$ $\implies$ $p=1$.
    \item Par exemple $A=[0,1[$ et $B=]0,1]$ sont deux tels événements car $A \cup B = [0,1]$ et $\PP{A \cap B} = \PP{]0,1[} = 1 = \PP{A}\PP{B}$.
  \end{enumerate}

\end{solution}

%-----------------------------------
\begin{exo}(Fonction de répartition)

  \begin{enumerate}
    \item Soit la fonction réelle
    \[
      F(x) = \frac{1+\atanh(x)}{2},
    \]
    où $\atanh(x) = \frac{\sinh(x)}{\cosh(x)}$ est l'arc tangente hyperbolique.

    \noindent Montrer que $F$ est la fonction de répartition d'une variable aléatoire réelle.
  \end{enumerate}
  \emph{On note $X$ une variable aléatoire qui a $F$ comme fonction de répartition.}
  \begin{enumerate}[resume]
    \item Quelle est la probabilité $\PP{X=a}$ pour $a \in \mathbb{R}$?
    \item Quelle est la probabilité $\PP{X \in [0,\ln(2)]}$?
    \item Soit $Y=X^{2}$. Montrer que la fonction de répartition $G(x)$ de $Y$ est
    \[
      G(x) =
      \begin{dcases}
        0                       & \text{si } x < 0,\\
        \atanh(\sqrt{x}) & \text{sinon.}
      \end{dcases}
    \]
  \end{enumerate}

\end{exo}

\begin{solution}
  \begin{enumerate}
    \item En utilisant que $\atanh$ est une fonction continue croissante avec $\lim_{-\infty}\atanh = -1$ et $\lim_{\infty}\atanh = 1$ on trouve que $F$ est une fonction continue\footnote{En particulier, continue à droite.} croissante avec $\lim_{-\infty}F = 0$ et $\lim_{\infty}F = 1$, donc c'est une fonction de répartition.
    \item Comme $F$ est continue nous avons $\PP{X=a}=0$ pour $\forall x \in \mathbb{R}$.
    \item En utilisant la question précédente on trouve $\PP{X \in [0,\ln(2)]} = F(\ln(2))-F(0)+\PP{X=0} = \frac{1+\atanh(\ln(2))}{2}-\frac{1+\atanh(0)}{2} + 0 = \frac{3}{10}$ car $\atanh(\ln(2)) = \frac{2-\frac{1}{2}}{2+\frac{1}{2}} = \frac{3}{5}$.
    \item Pour $x < 0$ nous avons $G(x) = \PP{X^2 \leq x} = \PP{\emptyset} = 0$. Pour $x \geq 0$ nous avons $G(x) = \PP{X^2 \leq x} = \PP{X \in [-\sqrt{x},\sqrt{x}]} = F(\sqrt{x}) - F(-\sqrt{x}) + \PP{X=-\sqrt{x}} = \frac{1+\atanh(\sqrt{x})}{2}-\frac{1+\atanh(-\sqrt{x})}{2} + 0$. Et comme $\atanh$ est une fonction impaire on trouve $G(x) = \atanh(\sqrt{x})$.
  \end{enumerate}
\end{solution}
\end{document}
