\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M54}

% pour voir les solutions il faut enlever le commentaire de la ligne suivante
 \solutionstrue

\newcommand*{\norm}[1]{\left\lVert{#1}\right\rVert} % la norme

\begin{document}


% ==================================
\hautdepage[-1]{
  \sisujet{Examen}\sisolutions{Solutions de l'examen}\\
  \normalfont\normalsize
  9 janvier 2018\\
  {[~16h30--18h30~]}
}
% ==================================
\vspace{-2mm}

\sisujet{
  \attention~\textbf{Seul document autorisé : le polycopié de Charles Suquet.}
  \vspace{5mm}
  \tsvp
}


%-----------------------------------
\begin{exo} (Inégalité de Cantelli)

  Soit $X$ une variable aléatoire réelle, d'espérance $\EE{X}=m$ et de variance $\VV{X} = \sigma^2$. Soit $t$ un réel strictement positif.
  \begin{enumerate}
    \item Soit $\alpha \geq 0$. Montrer que $\PP{X-m \geq t} = \PP{X-m+\alpha \geq t + \alpha}$.
    \item Calculer $\EE{(X-m+\alpha)^2}$.
    \item Montrer que pour tout $\alpha \geq 0$, on a
    \[
      \P (X-m \geq t) \leq \frac{\sigma^2 + \alpha^2}{t^2 + \alpha^2 + 2\alpha t}\cdot
    \]
    \item En déduire l'inégalité de Cantelli :
    \[
      \PP{X-m \geq t} \leq \frac{\sigma^2}{t^2 + \sigma^2}\cdot
    \]
    \item Montrer que
    \[
      \PP{|X-m| \geq t} \leq \frac{2 \sigma^2}{t^2 + \sigma^2}\cdot
    \]
    Dans quels cas cette inégalité est-elle meilleure que celle de Bienaymé-Tchebychev ?
    \item Soit $\alpha \in \ ]0,1[$ et $\varepsilon\in \ ]0,1/2[$. On lance $n$ fois une pièce de monnaie équilibrée. On note $X_1,\ldots, X_n$ les résultats des lancers avec $X_i = 1$ pour «face» et $X_i = 0$ pour «pile». On note
    \[
      \bar{X}_n = \frac{1}{n} (X_1 +\cdots + X_n)
    \]
    la fréquence d'apparition de «face». En utilisant la question précédente, déterminer un minorant de $n$ tel que $\bar{X}_n$ soit comprise entre $[\frac{1}{2} - \varepsilon, \frac{1}{2} + \varepsilon]$ avec une probabilité au moins égale à $\alpha$.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item\label{1a:sol} Comme $\alpha \geq 0$, on montre par double inclusion que les deux évènements $\{X-m \geq t\}$ et $\{X-m+\alpha \geq t + \alpha\}$ sont égaux, ce qui implique l'égalité des deux probabilités.
    \item On a :
      \begin{align*}
        \EE{(X-m+\alpha)^2} & = \EE{(X-m)^2 + 2\alpha(X-m) + \alpha^2}    \\
                            & = \EE{(X-m)^2} + 2\alpha\EE{X-m} + \alpha^2 \\
                            & = \sigma^2 + \alpha^2
      \end{align*}
    \item D'après la question \ref{1a:sol}), on a $\PP{X-m \geq t}= \PP{X-m+\alpha \geq t+\alpha}$. En appliquant l'inégalité de Markov à la variable aléatoire $Y^2$, où $Y=X-m+\alpha$, on en déduit :
      \[
        \PP{Y \geq t+\alpha} \leq \PP{Y^2 \geq (t+\alpha)^2} \leq \frac{\EE{Y^2}}{(t+\alpha)^ 2} = \frac{\sigma^2+\alpha^2}{t^2 + 2\alpha t + \alpha^2}
      \]
    \item On cherche à optimiser la borne obtenue. Pour cela, on cherche la valeur de $\alpha$ telle que $\frac{\sigma^2+\alpha^2}{t^2 + 2\alpha t + \alpha^2}$ soit minimale. On dérive alors cette expression par rapport à $\alpha$, et l'on cherche la valeur de $\alpha$ qui annule cette dérivée. Cela revient à annuler la quantité suivante : $2 \alpha (t+\alpha)^2 - 2 (\sigma^2 + \alpha^2)(t+\alpha) = 2(t+\alpha)(\alpha t -\sigma^2)$, qui s'annule en $\alpha = \sigma^2/t$.
    Ainsi, l'inégalité obtenue à la question précédente est optimale en $\alpha=\sigma^2/t$, et s'écrit alors :
      \begin{align*}
        \PP{X-m \geq t} & \leq \frac{\sigma^2+(\sigma^2/t)^2}{(t + \sigma^2/t)^2} \\
                        & \leq \frac{t^2 \sigma^2+\sigma^4}{(t^2 + \sigma^2)^2}   \\
                        & \leq \frac{\sigma^2}{t^2 + \sigma^2}.
      \end{align*}
    \item On a :
      \begin{align*}
        \PP{|X-m| \geq t} & =\PP{X-m \leq -t} +  \PP{X-m \geq t}     \\
                          & = \PP{-(X-m) \geq t} +  \PP{X-m \geq t}.
      \end{align*}
    En posant $Z=-X$, on a $\EE{Z}=\EE{-X}=-m$, et on peut appliquer le résultat de la question précédente pour montrer que $\PP{-(X-m) \geq t} = \PP{Z-\EE{Z} \geq t} \leq \frac{\sigma^2}{t^2+\sigma^2} $. On en déduit alors que
      \[
        \PP{|X-m| \geq t} \leq  \frac{2\sigma^2}{t^2+\sigma^2}.
      \]
    L'inégalité de Bienaymé-Tchebychev nous dit que :
      \[
        \PP{|X-m| \geq t} \leq \frac{\sigma^2}{t^2},
      \]
    donc l'inégalité de Cantelli est meilleure lorsque $\frac{2\sigma^2}{t^2+\sigma^2} \leq \frac{\sigma^2}{t^2}$, c'est-à-dire lorsque $t \leq \sigma$.
    \item Soit $X_n$ la variable aléatoire égale au nombre d'apparition de «face» après $n$ lancers d'une pièce de monnaie équilibrée. $X_n$ suit une loi binomiale de paramètres $n$ et $p=1/2$. On a $\EE{X_n}=np = \frac{n}{2}$, et $\text{Var}(X_n) = np(1-p) = \frac{n}{4}$. En remarquant que $\PP{X > t} \leq \PP{X \geq t}$, et en appliquant l'inégalité de la question \textbf{e)} à la variable aléatoire $X_n$, on obtient pour tout $t > 0$ :
      \[
        \PP{|X_n - \frac{n}{2}| > t} \leq 2 \ \frac{\frac{n}{4}}{t^2 + \frac{n}{4}} = 2 \frac{n}{4t^2 + n}.
      \]
    $X_n$ représente le nombre d'apparitions de «face», donc la \textit{fréquence} d'apparitions de «face» est donnée par $X_n/n$. En divisant par $n$ dans la probabilité précédente, et en prenant l'évènement complémentaire, on obtient :
      \[
        \PP{\left|\frac{X_n}{n} - \frac{1}{2}\right| \leq t/n} = \PP{\frac{1}{2} - \frac{t}{n} \leq \frac{X_n}{n} \leq \frac{1}{2}+ \frac{t}{n}} \geq 1-2 \frac{n}{4t^2 + n}.
      \]
    On pose $\varepsilon = t/n$, et on cherche $n$ tel que $1-2 \frac{n}{4t^2 + n}$ soit au moins égale à $\alpha$, c'est-à-dire tel que :
      \[
        \frac{4 n \varepsilon^2 - 1}{4 n \varepsilon^2 + 1} \geq \alpha.
      \]
    On en déduit la borne suivante sur $n$ :
      \[
        n \geq \frac{1}{4\varepsilon^2} \frac{1+\alpha}{1-\alpha}.
      \]
  \end{enumerate}
\end{solution}

% -----------------------------------
\begin{exo} (Variables aléatoires et lois)

  Soit $X$ la variable aléatoire continue dont la densité est
  \[
    \rho_X(t) = K\max(0,1-\abs{t}), \quad\text{pour } t\in \mathbb{R}.
  \]
  \begin{enumerate}
    \item Déterminer la valeur de la constante $K$.
    \item Déterminer la fonction de répartition $F_{X}$ de $X$.
    \item Calculer la probabilité $\PP{5X-2X^2\geq2}$.
    \item Soit $Y = \max(X,2)$. Déterminer la loi de $Y$.
    \item Est-ce que les variables $X$ et $Y$ sont indépendantes ?
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item\label{2a} On remarque tout d'abord que $1-\abs{t} > 0$ $\iff$ $\abs{t} < 1$ et donc
    \[
      \rho_X(t) = \left\{
        \begin{aligned}
          0\  & \text{\quad si} &         & \;t \leq -1 \\
          1+t & \text{\quad si} & -1 \leq & \;t \leq 0  \\
          1-t & \text{\quad si} &  0 \leq & \;t \leq 1  \\
          0\  & \text{\quad si} &  1 \leq & \;t
        \end{aligned}
       \right.\cdot\quad
       \raisebox{-.5\height}[0pt]{\includegraphics[width=7cm]{M54_2018-19_DS2_fig_exo2a_sol}}
    \]
    Ainsi
    \[
      \int_{-\infty}^{\infty}\rho_X(t) = K \int_{-1}^{0}(1+t)dt + K \int_{0}^{1}(1-t)dt = K \implies K=1.
    \]
    \item D'après la question précédente, comme $\int_{-1}^{t}(1+s)ds = \frac{1}{2}+t+\frac{t^2}{2}$ et $\int_{0}^{t}(1-s)ds = t-\frac{t^2}{2}$, nous trouvons
    \[
      F_X(t) =\left\{
        \begin{aligned}
          0\qquad                     & \quad\text{si} &         &\;t \leq -1 \\
          \frac{1}{2}+t+\frac{t^2}{2} & \quad\text{si} & -1 \leq &\;t \leq  0 \\
          \frac{1}{2}+t-\frac{t^2}{2} & \quad\text{si} &  0 \leq &\;t \leq  1 \\
          1\qquad                     & \quad\text{si} &  1 \leq &\;t
        \end{aligned}
      \right.\cdot\quad
       \raisebox{-.5\height}[0pt]{\includegraphics[width=7cm]{M54_2018-19_DS2_fig_exo2b_sol}}
    \]
    \item Nous avons $5X-2X^2\geq2$ $\iff$ $2X^2-5X+2\leq 0$ $\iff$ $X \in [\frac{1}{2}, 2]$. Ainsi $\PP{5X-2X^2\geq2} = \PP{\frac{1}{2}\leq X \leq 2} = F(2) - F(\frac{1}{2}) = \frac{1}{8}$.
    \item En utilisant la question précédente on trouve $\PP{Y=2} = \PP{X \leq 2} = 1$. Donc $Y$ est la variable constante $2$.
    \item Comme une variable constante est indépendante de toute autre variable \emph{(y compris d'elle même)}, les variables $X$ et $Y$ sont indépendantes.
  \end{enumerate}
\end{solution}

% -----------------------------------
\begin{exo} (Moments positifs et supremum essentiel)

  Soit $X$ une variable aléatoire positive. On suppose qu'il existe $K > 0$ telle que $\PP{X > K} = 0$ et $\PP{K-\varepsilon < X \le K} > 0$ pour tout $\varepsilon > 0$. On dit que $K$ est le \emph{supremum essentiel} de $X$. Enfin, pour tout $p\ge 1$, on note
  \[
    \norm{X}_p = \big(\EE{X^p}\big)^{1/p}.
  \]
  \begin{enumerate}
    \item Montrer que $\norm{X}_p \le K$ pour tout $p\ge 1$.
    \item En utilisant l'inégalité de Hölder, montrer que $p\mapsto \norm{X}_p$ est croissante et en déduire qu'elle converge vers une limite $\norm{X}_\infty$ inférieure à $K$.
    \item En utilisant la définition du supremum essentiel, montrer que pour tout $c\in \ ]0,1[$ et tout $\varepsilon > 0,$ il existe $p_0$ tel que pour tout $p\ge p_0,$ on ait $\norm{X}_p \ge c(K-\varepsilon)$. En déduire que $\norm{X}_\infty = K$.
    \item On suppose maintenant que $X$ est non bornée, autrement dit que $\PP{X > K} > 0$ pour tout $K > 0$. Montrer que $\norm{X}_p \to \infty$ quand $p\to\infty$.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item On a $\PP{X\le K} = 1$ et donc $\PP{X^p\le K^p} = 1$ pour tout $p\ge 1$. On en déduit
    \[
      \norm{X}_p^p \; =\;\int_0^{K^p} \PP{X^p > t}\, dt \; \le \; K^p,
    \]
    d'où le résultat.
    \item L'inégalité de Hölder, telle qu'elle est donnée en remarque 9.18 du poly et avait été énoncée en cours, signifie précisément que $p\mapsto \norm{X}_p$ est croissante. Comme elle est majorée par $K$, elle converge vers une limite $\norm{X}_\infty$ inférieure à $K$.
    \item Pour tout $\varepsilon > 0,$ il est clair que $\norm{X}_p^p\ge (K-\varepsilon)^p \PP{K-\varepsilon \le X \le K} = k(K-\varepsilon)^p$ avec $k = \PP{K-\varepsilon \le X \le K} > 0$ par hypothèse, indépendamment de $p$. On en déduit que $\norm{X}_p\ge k^{1/p} (K-\varepsilon)$ et comme $k^{1/p} = e^{(\log k)/p}\to 1$ quand $p\to \infty$ on voit que  $\norm{X}_\infty \ge K-\varepsilon$ pour tout $\varepsilon > 0,$ d'où $\norm{X}_\infty = K$.
    \item Si on pose $X_n =\inf(X,n),$ alors la question précédent montre que $\norm{X_n}_p \to n$ quand $p\to\infty$. D'autre part, on a toujours la croissance de  $p\mapsto \norm{X}_p$, et donc sa convergence vers une limite dans $[0,\infty]$ qui est minorée par $n$ pour tout $n$ puisque $\norm{X}_p\ge \norm{X_n}_p$. Donc cette limite est bien $\infty$.
  \end{enumerate}
\end{solution}

\end{document}
