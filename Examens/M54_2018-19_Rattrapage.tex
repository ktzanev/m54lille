\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M54}

% pour voir les solutions il faut enlever le commentaire de la ligne suivante
% \solutionstrue

\newcommand*{\norm}[1]{\left\lVert{#1}\right\rVert} % la norme

\begin{document}


% ==================================
\hautdepage[-1]{
  \sisujet{Rattrapage}\sisolutions{Solutions du rattrapage}\\
  \normalfont\normalsize
  13 juin 2019\\
  {[~10h30--12h30~]}
}
% ==================================
\vspace{-2mm}

\sisujet{
  \attention~\textbf{Seul document autorisé : le polycopié de Charles Suquet.}
  \vspace{14mm}
  \tsvp
}


%-----------------------------------
\begin{exo} (Dénombrement des permutations)

  On dispose d'un jeu de $52$ cartes neuves que l'on mélange au hasard.

  \begin{enumerate}
    \item Donner un ensemble $\Omega$ qui, muni de la loi uniforme, modélise cet événement aléatoire.
    \item Déterminer la probabilité qu'après avoir mélangé les cartes elles se retrouvent à leurs places initiales (comme si elles n'étaient pas mélangées).
    \item Déterminer la probabilité qu'après avoir mélangé les cartes l'as de pique ($A\spadesuit$) se retrouve à sa place initiale.
    \item Déterminer la probabilité qu'après avoir mélangé les cartes elles soient rangées par couleur: d'abord les $\spadesuit$, suivies des $\heartsuit$, puis les $\clubsuit$ et à la fin toutes les $\diamondsuit$.
    \item\hspace*{-4pt}\emph{[bonus]} Déterminer la probabilité qu'après avoir mélangé les cartes le roi de cœur ($R\heartsuit$) se retrouve juste derrière la dame de cœur ($D\heartsuit$).
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item On note $\mathcal{C} = \left\{ 2\spadesuit,3\spadesuit,\dots,R\diamondsuit,A\diamondsuit \right\}$ l'ensemble des 52 cartes. Ainsi l'ensemble $\Omega=\mathcal{P}(\mathcal{C})$ de toutes les permutations  de $\mathcal{C}$ convient, car mélanger au hasard les cartes consiste à appliquer une permutation au hasard.
    \item La probabilité qu'après avoir mélangé les cartes elles se retrouvent à leurs places initiales est la probabilité de choisir au hasard la permutation identité parmi les $\#\Omega = 52!$ permutations. Autrement dit cette probabilité est $\PP{\left\{ \operatorname{Id} \right\}} = \frac{1}{52!}$.
    \item Il y a $51!$ permutations qui laisse l'as de pique ($A\spadesuit$) à sa place initiale. Ainsi
    \[
      \PP{\text{«$A\spadesuit$ resta à sa place»}} = \frac{51!}{52!} = \frac{1}{52}.
    \]
    On aurait pu trouver cette probabilité en utilisant que toutes les $52$ places sont équiprobables pour $A\spadesuit$.
    \item Comme il y a $13$ cartes par couleur, le nombre de permutations qui rangent les quatre couleurs comme demandé est $13!\times 13!\times 13!\times 13!$. Ainsi la probabilité recherchée est $\frac{\left( 13! \right)^4}{52!} \approx 2\times10^{-29}$.
    \item Il y a $51$ positions possibles où le roi de cœur ($R\heartsuit$) est juste derrière la dame de cœur ($D\heartsuit$), et pour chacune de ses positions il y a $50!$ permutations qui la réalisent. Ainsi
    \[
      \PP{\text{«$R\heartsuit$ juste derrière $D\heartsuit$»}} = \frac{51 \times 50!}{52!} = \frac{1}{52}.
    \]
  \end{enumerate}
\end{solution}

% -----------------------------------
\begin{exo} (Fonctions de répartition et densité)

  Soient $\alpha >0$ et $\lambda >0$ deux réels strictement positifs, et soit $f_{\alpha,\lambda}$ la fonction définie sur $\mathbb{R}_+$ par :
  \[
  f_{\alpha,\lambda}(x) =  \frac{\alpha}{\lambda} \left(1- \frac{x}{\lambda}\right)^{\alpha-1} \ind_{[0,\lambda[}(x),
  \]
  où $\ind_{[0,\lambda[}$ est la fonction indicatrice de l'intervalle $[0,\lambda[$.
  \begin{enumerate}
    \item Montrer que $f_{\alpha,\lambda}$ définit bien une densité de probabilité.
  \end{enumerate}
  Soit $X$ une variable aléatoire dont la densité de probabilité est $f_{\alpha,\lambda}$.
  \begin{enumerate}[resume]
    \item Calculer $\mathbb{P}(X = \lambda)$.
    \item Quelle est la fonction de répartition de la variable $X$ ?
    \item Soit $Y = 1-\frac{X}{\lambda}\cdot$ Quelle est la densité de $Y$ ?
    \item\hspace*{-4pt}\emph{[bonus]} Soit $Z = Y^{\alpha}$. Quelle est la loi de $Z$ ?
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Comme $\alpha$ et $\lambda$ sont positifs, la fonction $f_{\alpha,\lambda}$ est positive sur $[0,\lambda[$. La fonction $f_{\alpha,\lambda}$ est bien intégrable sur $\mathbb{R}$, car $\alpha > 0$, donc $\alpha - 1 > -1$ et la fonction
    \[
      x \mapsto  \left(1- \frac{x}{\lambda}\right)^{\alpha-1}
    \]
    est donc bien intégrable entre $0$ et $\lambda$. Enfin, on a :
    \begin{align*}
      \int_{\mathbb{R}} f_{\alpha,\lambda}(x)dx & = \int_0^{\lambda} \frac{\alpha}{\lambda} \left(1-\frac{x}{\lambda}\right)^{\alpha-1}  dx \\
          & = \left[-\left(1-\frac{x}{\lambda}\right)^{\alpha}  \right]_0^{\lambda}  \\
          & = 1.
    \end{align*}
    La fonction $f_{\alpha,\lambda}$ vérifie donc bien les propriétés d'une densité de probabilité.
    \item Comme $X$ admet une densité, sa loi vérifie $\PP{X=a} = 0$ pour tout $a \in \mathbb{R}$. En particulier, $\PP{X=\lambda} = 0$.
    \item Notons $F_{\alpha,\lambda}$ la fonction de répartition de $X$. Comme $X$ est à valeurs dans $[0,\lambda[$, on a $F_{\alpha,\lambda}(x) = 0$ pour tout $x < 0$ et $F_{\alpha,\lambda}(x) = 1$ pour tout $x > \lambda$. Puis, par définition de la fonction de répartition, on a, pour tout $x \in [0,\lambda]$ :
    \begin{align*}
      F_{\alpha,\lambda} (x)
        & = \int_{-]\infty,x]} f_{\alpha,\lambda} (t) dt \\
        & = \int_0^x \frac{\alpha}{\lambda} \left( 1- \frac{t}{\lambda}\right)^{\alpha-1} dt \\
        & = \left[-\left(1-\frac{t}{\lambda}\right)^{\alpha} \right]_0^{x}  \\
        & = 1 - \left(1-\frac{x}{\lambda}\right)^{\alpha}.
    \end{align*}
    Finalement, on a donc $F_{\alpha,\lambda} (x) =  (1 - \left(1-\frac{x}{\lambda}\right)^{\alpha}) \ind_{[0,\lambda[} (x)\,+\, \ind_{[\lambda, +\infty[}(x)$.
    \item Soit $F_Y$ la fonction de répartition de $Y$. La variable aléatoire $Y$ est à valeurs dans $[0,1]$, donc $F_y(y) = 0$ pour tout $y < 0$ et $F_y(y) = 1$ pour tout $y > 1$. Puis, pour $y \in [0,1]$, on a :
    \begin{align*}
      F_Y(y) & = \PP{Y \leq y} \\
             & = \PP{X \geq \lambda (1-y)} \\
             & = y^\alpha.
    \end{align*}
    Donc la densité de $Y$ est $F_Y'(y) = \alpha y^{\alpha-1}\ind_{]0,1[}(y)$.
    \item De même, la densité de $Z$ est $\ind_{]0,1[}(z)$. On reconnaît la densité uniforme sur $]0,1[$.
  \end{enumerate}
\end{solution}


% -----------------------------------
\begin{exo} (Moments)

Soit $X$ une variable aléatoire réelle. On appelle \emph{fonction génératrice des moments} la fonction définie par :
\[
   M_X(t) = \EE{e^{tX}},
\]
pour tout $t \in  \mathbb{R}$ tel que cette espérance soit finie. La fonction génératrice des moments caractérise la loi : \emph{deux variables aléatoires $X_1$ et $X_2$ ont même fonction génératrice des moments si et seulement si elles ont la même loi.}

\begin{enumerate}
  \item Exprimer $M_Y$, la fonction génératrice des moments de $Y = aX + b$, pour $a,b$ réels quelconques, en fonction de $M_X$.
  \item Calculer la fonction génératrice des moments dans le cas où $X$ suit une loi normale de moyenne 0 et de variance 1. En déduire la fonction génératrice des moments d'une variable aléatoire de loi normale de moyenne $\mu$ et de variance $\sigma^2$.
  \item Calculer la fonction génératrice des moments dans le cas où $X$ suit une loi de Poisson de paramètre $\lambda$.
  \item Soit $X_1, \dots, X_n$ des variables aléatoires indépendantes et identiquement distribuées de loi de Poisson de paramètre $\lambda$.
  \begin{enumerate}
    \item Calculer la fonction génératrice des moments de $S_n = X_1 + \dots + X_n$. Quelle est la loi de $S_n$ ?
    \item On considère la variable aléatoire $Y_n =  \frac{S_n - n\lambda}{\sqrt{n\lambda}}$. Calculer $M_{Y_n}$ et sa limite lorsque $n$ tend vers $+\infty$. Quel résultat a t-on en fait démontré ici ?
    \item\hspace*{-4pt}\emph{[bonus]} Que peut-on dire lorsque les variables $X_1, \dots, X_n$ sont indépendantes de loi de Poisson de paramètre $\lambda_i$ ? Quelle condition doit-on imposer sur la suite $(\lambda_i)$ pour obtenir le même résultat ?
  \end{enumerate}
\end{enumerate}

\end{exo}

\begin{solution}
\begin{enumerate}
  \item $M_Y(t) = \EE{e^{(aX+b)t)}} = \EE{e^{(at)X} e^{bt}} = e^{bt} M_X(at)$.
  \item
  \begin{align*}
    M_X(t) & = \int_{\mathbb{R}} e^{tx} \frac{1}{\sqrt{2\pi}} e^{-x^2/2}dx \\
                 & = \int_{\mathbb{R}} \frac{1}{\sqrt{2\pi}} e^{-x^2/2 + tx}dx \\
                   & = \int_{\mathbb{R}} \frac{1}{\sqrt{2\pi}} e^{-\frac{1}{2}(x-t)^2 + t^2/2}dx \\
                 & = e^{t^2/2} \int_{\mathbb{R}} \frac{1}{\sqrt{2\pi}} e^{-(x-t)^2/2}dx \\
                 & = e^{t^2/2},
  \end{align*}
  où le passage de l'avant-dernière à la dernière ligne se justifie en remarquant que l'intégrale vaut 1 car il s'agit de l'intégrale de la densité d'une loi normale de moyenne $t$ et de variance 1.

  Soit $Z = \mu + \sigma X$. Alors $Z$ suit une loi normale de moyenne $\mu$ et de variance $\sigma^2$. On a $M_Z(t) = e^{\mu t} M_X(\sigma t) = e^{\mu t} e^{\sigma^2 t^2 /2}.$
  \item
  \begin{align*}
    M_X(t) & = \sum_{k=0}^n e^{tk} \PP{X = k} \\
              & = \sum_{k=0}^n e^{tk} e^{-\lambda} \frac{\lambda^k}{k!}  \\
              & = e^{-\lambda} \sum_{k=0}^n \frac{(e^t \lambda)^k}{k!}  \\
              & = e^{-\lambda} e^{e^t \lambda} \\
              & = e^{\lambda(e^t -1)}.
  \end{align*}

  \item
  \begin{enumerate}
    \item $M_{S_n} (t) = \EE{e^{X_1 + \dots + X_n}} = \EE{e^{X_1} \dots e^{X_n}} = \EE{e^{X_1}} \dots \EE{e^{X_n}} $, par indépendance des $X_i$. Les $X_i$ ayant tous la même loi de Poisson de paramètre $\lambda$, et en utilisant la question précédente, on en déduit :
  $$M_{S_n}(t) = \prod_{i=1}^n e^{\lambda(e^t -1)} = e^{\lambda n (e^t -1)}.$$
  Donc $S_n$ suit une loi de Poisson de paramètre $n\lambda$.

    \item $$M_{Y_n}(t) = e^{-\sqrt{n\lambda}t} M_{S_n}(t/\sqrt{n\lambda}) =  e^{-\sqrt{n\lambda}t}  e^{\lambda n (e^{t/\sqrt{n\lambda}} -1)}.$$
    Lorsque $n$ est grand, on peut approcher $e^{t/\sqrt{n\lambda}}$ par son développement limité, avec $e^{t/\sqrt{n\lambda}} \approx 1 + \frac{t}{\sqrt{n\lambda}} + \frac{t^2}{2n\lambda}$. On a alors :
    $$M_{Y_n}(t) \approx e^{-\sqrt{n\lambda}t} e^{\lambda n (1+\frac{t}{\sqrt{n\lambda}} + \frac{t^2}{2n\lambda}-1)} \approx e^{t^2/2}.$$
    Autrement dit, lorsque $n \rightarrow + \infty$, la f.g.m.\footnote{fonction génératrice des moments} de $Y_n$ converge vers la f.g.m. d'une variable aléatoire normale de moyenne 0 et de variance 1. On retrouve en fait le résultat du TCL\footnote{théorème central limite}, appliqué à une suite de v.a.i.i.d.\footnote{variables aléatoires indépendantes et identiquement distribuées} de loi de Poisson de paramètre $\lambda$ (le TCL s'applique bien dans ce cas car les variables sont de variance finie).

    \item Le raisonnement précédent reste valable lorsque les variables sont indépendantes (nécessaire pour passer de l'espérance du produit au produit des espérances), mais le caractère "identiquement distribuées" n'est pas nécessaire. En effet, dans ce cas $S_n$ suit une loi de Poisson de paramètre $\sum_i \lambda_i$. On pose alors $Z_n = \frac{S_n - \sum_{i=1}^n \lambda_i}{\sqrt{\sum_{i=1}^n \lambda_i}}$, et on peut montrer selon un raisonnement similaire que $M_{Z_n} \rightarrow e^{t^2/2}$ lorsque $n \rightarrow + \infty$. Il suffit d'avoir $\sum_i\lambda_i \rightarrow + \infty$ pour justifier le DL précédent et obtenir la convergence en loi vers la loi normale centrée réduite.
  \end{enumerate}
\end{enumerate}

\end{solution}



\end{document}
